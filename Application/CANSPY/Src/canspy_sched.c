/**
  ******************************************************************************
  * @file    canspy_sched.c
  * @author  Arnaud Lebrun <a-lebrun@live.fr>
  * @author  Jonathan-Christofer Demay <jcdemay@gmail.com>
  * @version V1.0
  * @brief   Sched module core.
  *          This file provides all the functions to manage the scheduler:
  *           + Initializing
  *           + Running
	*           + Consuming flags
	*           + Retreiving buffers
  *
  ******************************************************************************
  * COPYRIGHT(c) 2016 Arnaud Lebrun and Jonathan-Christofer Demay
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of the copyright holder nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  ******************************************************************************
  */ 

#include "canspy_sched.h"
#include "canspy_debug.h"

/** @brief  The storage for all registered services
  */
CANSPY_DEVICE_DESC CANSPY_SCHED_Devices[CANSPY_DEVICES];

/** @brief  All the flags stored in an array
  */
uint16_t CANSPY_SCHED_Flags[CANSPY_FLAGS];

/** @brief  Signal that there is new set flags
  */
bool CANSPY_SCHED_Interrupt = true;

/** @brief  Signal that the scheduler is running
  */
bool CANSPY_SCHED_Running = false;

/**
  * @brief  Initialize the scheduler
  * @retval None
  */
void canspy_sched_init(){
	
	CANSPY_SERVICE_FLAG f;
	CANSPY_DEVICE_ID d;
	
	for(f = CANSPY_FLAG_FIRST; f < CANSPY_FLAGS; f++)
		CANSPY_SCHED_Flags[f] = 0;
	
	for(d = CANSPY_DEVICE_FIRST; d < CANSPY_DEVICES; d++){
		
		CANSPY_DEVICE_POINTER(d)->dev_id = CANSPY_NODEVICE;
		CANSPY_DEVICE_POINTER(d)->dev_func = NULL;
		CANSPY_DEVICE_POINTER(d)->dev_opt->var_list = NULL;
		CANSPY_DEVICE_POINTER(d)->dev_opt->var_size = 0;
		CANSPY_DEVICE_POINTER(d)->dev_name = NULL;
		CANSPY_DEVICE_POINTER(d)->dev_handle = NULL;
		CANSPY_DEVICE_POINTER(d)->powered = false;
		CANSPY_DEVICE_POINTER(d)->paused = false;
		CANSPY_DEVICE_POINTER(d)->svc_list = NULL;
		CANSPY_DEVICE_POINTER(d)->svc_length = 0;
		CANSPY_DEVICE_POINTER(d)->svc_sync = 0;
		CANSPY_DEVICE_POINTER(d)->svc_async = 0;
	}
}

/**
  * @brief  Execute the scheduler.
  * @retval None
  */
void canspy_sched_exec(void){

	uint16_t i;
	int nomore;
	bool found_excl;
	CANSPY_DEVICE_ID d;
	CANSPY_SERVICE_FLAG f;
	CANSPY_OPTION_CALL opt_call;
	
	CANSPY_SCHED_Running = true;
	
	while(CANSPY_SCHED_Running){
		
		nomore = -1;
		
		/* Start scheduling synchrone services until there is no set flags */
		while(nomore != 0){
			
			nomore = 0;

			for(f = CANSPY_FLAG_FIRST; f < CANSPY_FLAGS; f++){
			
				if(CANSPY_SCHED_Flags[f] > 0){
					
					nomore++;
					
					for(d = CANSPY_DEVICE_FIRST; d < CANSPY_DEVICES; d++){
						
						if(CANSPY_DEVICE_ACTIVE(d) && CANSPY_DEVICE_POINTER(d)->svc_sync > 0){

							found_excl = false;
								
							for(i = 0; i < CANSPY_DEVICE_POINTER(d)->svc_length; i++){
									
								if(CANSPY_DEVICE_POINTER(d)->svc_list[i].started
									&& !CANSPY_DEVICE_POINTER(d)->svc_list[i].asynchrone
									&& (!CANSPY_DEVICE_POINTER(d)->svc_list[i].exclusive || !found_excl)){
									
									opt_call.svc_ptr = &(CANSPY_DEVICE_POINTER(d)->svc_list[i]);
									CANSPY_DEVICE_POINTER(d)->svc_list[i].svc_func(f, &opt_call);
									
									/* Ensure that only the first activated exclusive service is executed */
									if(CANSPY_DEVICE_POINTER(d)->svc_list[i].exclusive)
										found_excl = true;
								}
							}
						}
					}

					canspy_sched_consume(f);
					HAL_GPIO_WritePin(GPIOD, GPIO_PIN_15, GPIO_PIN_RESET);
				}
			}
		}

		CANSPY_SCHED_Interrupt = false;
		
		/* Start scheduling asynchrone services until there is new set flags */
		while(!CANSPY_SCHED_Interrupt){
			
			for(d = CANSPY_DEVICE_FIRST; d < CANSPY_DEVICES && !CANSPY_SCHED_Interrupt; d++){
					
				if(CANSPY_DEVICE_ACTIVE(d) && CANSPY_DEVICE_POINTER(d)->svc_async > 0 && !CANSPY_SCHED_Interrupt){
						
					for(i = 0; i < CANSPY_DEVICE_POINTER(d)->svc_length && !CANSPY_SCHED_Interrupt; i++){
							
						if(CANSPY_DEVICE_POINTER(d)->svc_list[i].started && CANSPY_DEVICE_POINTER(d)->svc_list[i].asynchrone && !CANSPY_SCHED_Interrupt){
							
							opt_call.svc_ptr = &(CANSPY_DEVICE_POINTER(d)->svc_list[i]);
							CANSPY_DEVICE_POINTER(d)->svc_list[i].svc_func(CANSPY_FLAGS, &opt_call);
						}
					}
				}
			}
		}
	}
}

/**
  * @brief  Internal processing of flags and buffers
  * @param  flag: the flag associated with a buffer.
  * @param  reset: specifies to consume the flag
  * @retval The corresponding buffer or NULL
  */
void *canspy_sched_internal(CANSPY_SERVICE_FLAG flag, bool reset){

	static bool can_dup_set[CAN2_RX] = {false, false};
	static CanRxMsgTypeDef can_dup_buf[CAN2_RX];
	static void *can_dup_ptr[CAN2_RX] = {&CAN1_Rx_Mess, &CAN2_Rx_Mess};
	uint16_t old_val;
	
	if(reset){
		
		switch(flag){
			
			case UART_TX:
			case UART_RX:
			case UART_BREAK:
				CANSPY_SCHED_Flags[flag]--;
				break;
			case ETH_RX:
				CANSPY_SCHED_Flags[flag] = 0;
				break;
			case CAN1_RX:
			case CAN2_RX:
				__disable_irq();
				if((old_val = CANSPY_SCHED_Flags[flag]) > 1 && can_dup_set[flag - 1])
					CANSPY_SCHED_Flags[flag] = 1;
				else
					CANSPY_SCHED_Flags[flag] = 0;
				can_dup_set[flag - 1] = false;
				old_val = old_val - 1 - CANSPY_SCHED_Flags[flag];
				__enable_irq();
				if(old_val > 0){
					
					CANSPY_DEBUG_COUNT_Miss[flag - 1] += old_val;
					CANSPY_DEBUG_ERROR_2(INFO, "%i missed frames from CAN%i", old_val, flag);
				}
				break;
			default:
				CANSPY_SCHED_Flags[flag] = 0;
				break;
		}
		
		return NULL;
	}
	else{
		
		switch(flag){
			
			case ETH_RX:
				return ETH_Rx_Buf[0];
			case CAN1_RX:
			case CAN2_RX:
				if(!can_dup_set[flag - 1]){
					
					__disable_irq();
					memcpy(&(can_dup_buf[flag - 1]), can_dup_ptr[flag - 1], sizeof(can_dup_buf[flag - 1]));
					can_dup_set[flag - 1] = true;
					__enable_irq();
				}
				return &(can_dup_buf[flag - 1]);
			default:
				return NULL;
		}
	}
}

/**
  * @brief  Retreive a buffer associated with a flag.
  * @param  flag: the associated flag.
  * @retval The corresponding buffer
  */
void *canspy_sched_buffer(CANSPY_SERVICE_FLAG flag){
	
	return canspy_sched_internal(flag, false);
}

/**
  * @brief  Consume a flag.
  * @param  flag: the flag to consume.
  * @retval None
  */
void canspy_sched_consume(CANSPY_SERVICE_FLAG flag){
	
	canspy_sched_internal(flag, true);
}
