/**
  ******************************************************************************
  * @file    canspy_cmd.c
  * @author  Arnaud Lebrun <a-lebrun@live.fr>
  * @author  Jonathan-Christofer Demay <jcdemay@gmail.com>
  * @version V1.0
  * @brief   CMD module extension.
  *          This file provides all the commands needed by the SHELL service:
  *           + Device management
	*           + Service management
	*           + Option management
	*           + Filter management
	*           + CAN injection
	*           + File management
  *
  ******************************************************************************
  * COPYRIGHT(c) 2016 Arnaud Lebrun and Jonathan-Christofer Demay
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of the copyright holder nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  ******************************************************************************
  */ 

#include "canspy_cmd.h"
#include "canspy_shell.h"
#include "canspy_uart.h"
#include "canspy_can.h"
#include "canspy_eth.h"
#include "canspy_filter.h"
#include "canspy_sdcard.h"
#include "canspy_pcap.h"
#include "canspy_print.h"
#include "canspy_debug.h"

/** @brief  Shared variable to reduce the use of the stack.
  */
CANSPY_SDCARD_FIL CANSPY_CMD_FIL_Shared;

/**
  * @brief  The help command.
  * @param  argc/argv: the standard input for commands.
  * @retval None
  */
void canspy_cmd_help(int argc, char **argv){
	
	int i;
	
	for(i = 0; i < CANSPY_SHELL_Commands.size; i++){

		if(argc == 2){
			
			if(strncmp(CANSPY_SHELL_Commands.list[i].name, argv[1], CANSPY_PRINT_MAX_Command) == 0){

				canspy_print_format_line(CANSPY_HANDLE_UART, "%s", CANSPY_SHELL_Commands.list[i].desc);
				canspy_print_format_line(CANSPY_HANDLE_UART, "%s %s", CANSPY_SHELL_Commands.list[i].name, CANSPY_SHELL_Commands.list[i].help);
				break;
			}
		}
		else{
			
			if(i == 0){
				
				canspy_print_padding_string(CANSPY_HANDLE_UART, "COMMAND", 0, &CANSPY_PRINT_MAX_Command, 2);
				CANSPY_UART_STATIC_LINE("DESCRIPTION");	
			}
			
			canspy_print_padding_string(CANSPY_HANDLE_UART, CANSPY_SHELL_Commands.list[i].name, 0, &CANSPY_PRINT_MAX_Command, 2);
			canspy_print_format_line(CANSPY_HANDLE_UART, "%s", CANSPY_SHELL_Commands.list[i].desc);
		}
	}
}

/**
  * @brief  The stats command.
  * @param  argc/argv: the standard input for commands.
  * @retval None
  */
void canspy_cmd_stats(int argc, char **argv){

	CANSPY_FILTER_CAN fil_can;
	bool reset;
		
	if(argc != 3){

		CANSPY_UART_STATIC_LINE("missing argument");
		return;
	}
	
	if(strcasecmp(CANSPY_FILTER_CAN_String[CAN_IF1], argv[2]) == 0)
		fil_can = CAN_IF1;
	else if(strcasecmp(CANSPY_FILTER_CAN_String[CAN_IF1], argv[2]) == 0)
		fil_can = CAN_IF2;
	else if(strcasecmp(CANSPY_FILTER_CAN_String[CAN_ALL], argv[2]) == 0)
		fil_can = CAN_ALL;
	else{
		
		CANSPY_UART_STATIC_LINE("invalid CAN interface");
		return;
	}
	
	if(STRNICONSTCMP("list", argv[1]) == 0)
		reset = false;
	else if(STRNICONSTCMP("reset", argv[1]) == 0)
		reset = true;
	else{

		CANSPY_UART_STATIC_LINE("invalid argument");
		return;
	}
	
	if(fil_can == CAN_IF1 || fil_can == CAN_ALL)
		canspy_debug_stats(CANSPY_HANDLE_UART, CAN_IF1, reset);
		
	if(fil_can == CAN_IF2 || fil_can == CAN_ALL)
		canspy_debug_stats(CANSPY_HANDLE_UART, CAN_IF2, reset);
}

/**
  * @brief  The device command.
  * @param  argc/argv: the standard input for commands.
  * @retval None
  */
void canspy_cmd_device(int argc, char **argv){

	int ret, found;
	char *result;
	CANSPY_DEVICE_DESC *cur_dev = NULL;
	CANSPY_SERVICE_DESC *cur_svc = NULL;
	
	if(argc == 2 && STRNICONSTCMP("list", argv[1]) == 0){
			
		canspy_print_padding_string(CANSPY_HANDLE_UART, "DEVICE", 0, &CANSPY_PRINT_MAX_Device, CANSPY_PRINT_PAD_Device);
		canspy_print_format_line(CANSPY_HANDLE_UART, "STATE\t\tACTIVE");
		
		while((cur_dev = canspy_device_walk(cur_dev)) != NULL){
			
			canspy_print_padding_string(CANSPY_HANDLE_UART, cur_dev->dev_name, 0, &CANSPY_PRINT_MAX_Device, CANSPY_PRINT_PAD_Device);
			canspy_print_format(CANSPY_HANDLE_UART, "%s\t\t", cur_dev->powered ? (cur_dev->paused ? "paused" : "started") : "stopped");
			
			if(cur_dev->svc_sync + cur_dev->svc_async> 0){
				
				found = 0;
				while((cur_svc = canspy_device_active(cur_dev, cur_svc)) != NULL){
				
					if(found++ > 0)
						CANSPY_UART_STATIC_STRING(",");
					canspy_print_format(CANSPY_HANDLE_UART, "%s", cur_svc->svc_name);
				}
				
				CANSPY_UART_BREAK();
			}
			else
				CANSPY_UART_STATIC_LINE("none");
		}
	}
	else if(argc == 3){
		
		while((cur_dev = canspy_device_walk(cur_dev)) != NULL)
			if(strncasecmp(cur_dev->dev_name, argv[2], CANSPY_PRINT_MAX_Device) == 0)
				break;

		if(cur_dev == NULL){
			
			canspy_print_format_line(CANSPY_HANDLE_UART, "unknown device %s", argv[2]);
			return;
		}
	
		if(STRNICONSTCMP("status", argv[1]) == 0){
			
			canspy_print_format(CANSPY_HANDLE_UART, "%s %s", cur_dev->dev_name, cur_dev->powered ? (cur_dev->paused ? "paused" : "started") : "stopped");
			
			if(cur_dev->svc_sync + cur_dev->svc_async> 0){
				
				found = 0;
				CANSPY_UART_STATIC_STRING(" (");
				while((cur_svc = canspy_device_active(cur_dev, cur_svc)) != NULL){
				
					if(found++ > 0)
						CANSPY_UART_STATIC_STRING(",");
					canspy_print_format(CANSPY_HANDLE_UART, "%s", cur_svc->svc_name);
				}
				CANSPY_UART_STATIC_STRING(")");
			}
			
			CANSPY_UART_BREAK();
			return;
		}
		else if(STRNICONSTCMP("start", argv[1]) == 0){
			
			ret = canspy_device_start(cur_dev);
			result = "started";
		}
		else if(STRNICONSTCMP("pause", argv[1]) == 0){
			
			ret = canspy_device_pause(cur_dev);
			result = "paused";
		}
		else if(STRNICONSTCMP("resume", argv[1]) == 0){
			
			ret = canspy_device_resume(cur_dev);
			result = "resumed";
		}
		else if(STRNICONSTCMP("stop", argv[1]) == 0){
			
			ret = canspy_device_stop(cur_dev);
			result = "stopped";
		}
		else{
			
			CANSPY_UART_STATIC_LINE("invalid argument");
			return;
		}
		
		if(ret == EXIT_SUCCESS)
			canspy_print_format_line(CANSPY_HANDLE_UART, "device %s successfully %s", cur_dev->dev_name, result);
		else
			canspy_print_format_line(CANSPY_HANDLE_UART, "%s: hardware error or device already %s", argv[2], result);
	}
	else
		CANSPY_UART_STATIC_LINE("invalid syntax, see help");
}

/**
  * @brief  The service command.
  * @param  argc/argv: the standard input for commands.
  * @retval None
  */
void canspy_cmd_service(int argc, char **argv){
	
	CANSPY_DEVICE_DESC *cur_dev = NULL;
	CANSPY_SERVICE_DESC *cur_svc = NULL;
	char *state;
	
	if(argc == 2 && STRNICONSTCMP("list", argv[1]) == 0){

		canspy_print_padding_string(CANSPY_HANDLE_UART, "SERVICE", 0, &CANSPY_PRINT_MAX_Service, CANSPY_PRINT_PAD_Service);
		canspy_print_padding_string(CANSPY_HANDLE_UART, "DEVICE", 0, &CANSPY_PRINT_MAX_Device, CANSPY_PRINT_PAD_Device);
		CANSPY_UART_STATIC_LINE("STATE\t\tEXCLU\t\tASYNC\t\tDESCRIPTION");
		
		while((cur_dev = canspy_device_walk(cur_dev)) != NULL){
			
			CANSPY_UART_STATIC_LINE("-");
			bool found_exclusive = false;
			
			while((cur_svc = canspy_service_walk(cur_dev, cur_svc)) != NULL){
				
				if(!cur_svc->started)
						state = "stopped";
				else{
					
					if(cur_dev->paused)
						state = "paused";
					else if(!cur_svc->exclusive)
						state = "started";
					else if(found_exclusive)
						state = "paused";
					else{
						
						state = "started";
						found_exclusive = true;
					}				
				}

				canspy_print_padding_string(CANSPY_HANDLE_UART, cur_svc->svc_name, 0, &CANSPY_PRINT_MAX_Service, CANSPY_PRINT_PAD_Service);
				canspy_print_padding_string(CANSPY_HANDLE_UART, cur_dev->dev_name, 0, &CANSPY_PRINT_MAX_Device, CANSPY_PRINT_PAD_Device);
				canspy_print_format_line(CANSPY_HANDLE_UART, "%s\t\t%s\t\t%s\t\t%s", state, cur_svc->exclusive ? CANSPY_OPTION_BOOL_String[true] : CANSPY_OPTION_BOOL_String[false], cur_svc->asynchrone ? CANSPY_OPTION_BOOL_String[true] : CANSPY_OPTION_BOOL_String[false], cur_svc->svc_desc);
			}
		}
	}
	else if(STRNICONSTCMP("status", argv[1]) == 0){
		
		if((cur_svc = canspy_service_find(CANSPY_NODEVICE, argv[2], NULL)) != NULL){
			
			canspy_print_format_line(CANSPY_HANDLE_UART, "%s:%s", cur_svc->svc_name,
				!cur_svc->started ? "stopped" : (!CANSPY_DEVICE_ACTIVE(cur_svc->dev_ptr->dev_id) ? "paused" : ((cur_svc->svc_func != canspy_service_find(cur_svc->dev_ptr->dev_id, NULL, NULL)->svc_func && cur_svc->exclusive)? "paused" : "started")));
		}
		else
			canspy_print_format_line(CANSPY_HANDLE_UART, "%s: unknown service", argv[2]);
	}
	else if(argc == 3 && STRNICONSTCMP("start", argv[1]) == 0){
			
		if(canspy_service_start(NULL, CANSPY_NODEVICE, argv[2], NULL) == EXIT_SUCCESS)
			canspy_print_format_line(CANSPY_HANDLE_UART, "service %s successfully started", argv[2]);
		else
			canspy_print_format_line(CANSPY_HANDLE_UART, "%s: unknown service or service already started", argv[2]);
	}
	else if(argc == 3 && STRNICONSTCMP("stop", argv[1]) == 0){
		
		if(canspy_service_stop(NULL, CANSPY_NODEVICE, argv[2], NULL) == EXIT_SUCCESS)
			canspy_print_format_line(CANSPY_HANDLE_UART, "service %s successfully stopped", argv[2]);
		else
			canspy_print_format_line(CANSPY_HANDLE_UART, "%s: unknown service or service already stopped", argv[2]);
	}
	else
		CANSPY_UART_STATIC_LINE("invalid syntax, see help");
}

/**
  * @brief  The option command.
  * @param  argc/argv: the standard input for commands.
  * @retval None
  */
void canspy_cmd_option(int argc, char **argv){

	CANSPY_DEVICE_DESC *cur_dev = NULL;
	CANSPY_SERVICE_DESC *cur_svc = NULL;
	CANSPY_OPTION_CMP *cur_var = NULL;
	
	if(argc == 2 && STRNICONSTCMP("list", argv[1]) == 0){

		canspy_print_padding_string(CANSPY_HANDLE_UART, "OPTION", 0, &CANSPY_PRINT_MAX_Option, CANSPY_PRINT_PAD_Option);
		canspy_print_padding_string(CANSPY_HANDLE_UART, "VALUE", 0, &CANSPY_PRINT_MAX_Value, CANSPY_PRINT_PAD_Value);
		canspy_print_padding_string(CANSPY_HANDLE_UART, "RANGE", 0, &CANSPY_PRINT_MAX_Value, CANSPY_PRINT_PAD_Range);
		canspy_print_padding_string(CANSPY_HANDLE_UART, "DEVICE", 0, &CANSPY_PRINT_MAX_Device, CANSPY_PRINT_PAD_Device);
		canspy_print_padding_string(CANSPY_HANDLE_UART, "SERVICE", 0, &CANSPY_PRINT_MAX_Service, CANSPY_PRINT_PAD_Service);
		CANSPY_UART_STATIC_LINE("RESTART\t\tDESCRIPTION");
		
		while((cur_dev = canspy_device_walk(cur_dev)) != NULL){
			
			CANSPY_UART_STATIC_LINE("-");
			
			while((cur_var = canspy_option_walk(cur_dev->dev_opt, cur_var, cur_dev->dev_func)) != NULL){
				
				canspy_print_padding_string(CANSPY_HANDLE_UART, cur_var->var_name, 0, &CANSPY_PRINT_MAX_Option, CANSPY_PRINT_PAD_Option);
				canspy_print_padding_string(CANSPY_HANDLE_UART, cur_var->var_string, 0, &CANSPY_PRINT_MAX_Value, CANSPY_PRINT_PAD_Value);
				canspy_print_padding_string(CANSPY_HANDLE_UART, cur_var->var_range, 0, &CANSPY_PRINT_MAX_Range, CANSPY_PRINT_PAD_Range);
				canspy_print_padding_string(CANSPY_HANDLE_UART, cur_dev->dev_name, 0, &CANSPY_PRINT_MAX_Device, CANSPY_PRINT_PAD_Device);
				canspy_print_padding_string(CANSPY_HANDLE_UART, "ALL", 0, &CANSPY_PRINT_MAX_Service, CANSPY_PRINT_PAD_Service);
				canspy_print_format_line(CANSPY_HANDLE_UART, "%s\t\t%s", cur_var->restart ? "yes" : "no", cur_var->var_desc);
			}
			
			while((cur_svc = canspy_service_walk(cur_dev, cur_svc)) != NULL){
				
				while((cur_var = canspy_option_walk(cur_svc->svc_opt, cur_var, cur_svc->svc_func)) != NULL){
					
					canspy_print_padding_string(CANSPY_HANDLE_UART, cur_var->var_name, 0, &CANSPY_PRINT_MAX_Option, CANSPY_PRINT_PAD_Option);
					canspy_print_padding_string(CANSPY_HANDLE_UART, cur_var->var_string, 0, &CANSPY_PRINT_MAX_Value, CANSPY_PRINT_PAD_Value);
					canspy_print_padding_string(CANSPY_HANDLE_UART, cur_var->var_range, 0, &CANSPY_PRINT_MAX_Range, CANSPY_PRINT_PAD_Range);
					canspy_print_padding_string(CANSPY_HANDLE_UART, cur_dev->dev_name, 0, &CANSPY_PRINT_MAX_Device, CANSPY_PRINT_PAD_Device);
					canspy_print_padding_string(CANSPY_HANDLE_UART, cur_svc->svc_name, 0, &CANSPY_PRINT_MAX_Service, CANSPY_PRINT_PAD_Service);
					canspy_print_format_line(CANSPY_HANDLE_UART, "%s\t\t%s", cur_var->restart ? "yes" : "no", cur_var->var_desc);
				}
			}
		}
	}
	else if(argc == 3 && STRNICONSTCMP("get", argv[1]) == 0){

		if((cur_var = canspy_option_variable(false, argv[2], NULL)) != NULL)
			canspy_print_format_line(CANSPY_HANDLE_UART, "%s:\"%s\"", cur_var->var_name, cur_var->var_string);
		else
			CANSPY_UART_STATIC_LINE("invalid option name");
	}
	else if(argc == 4 && STRNICONSTCMP("set", argv[1]) == 0){
		
		 if((cur_var = canspy_option_variable(true, argv[2], argv[3])) != NULL)
			canspy_print_format_line(CANSPY_HANDLE_UART, "%s:\"%s\"", cur_var->var_name, cur_var->var_string);
		 else
			 CANSPY_UART_STATIC_LINE("invalid option name or value");
	}
	else
		CANSPY_UART_STATIC_LINE("invalid syntax, see help");
}

/**
  * @brief  The filter command.
  * @param  argc/argv: the standard input for commands.
  * @retval None
  */
void canspy_cmd_filter(int argc, char **argv){
	
	uint8_t rul_id;
	CANSPY_FILTER_RULE *cur_rul = NULL;

	if(argc == 2 && STRNICONSTCMP("help", argv[1]) == 0){
		
		char *args[] = {
			"IF:     [CAN1|CAN2]",
			"ID:     [=|>|<|!][UINT]",
			"TYPE:   [RTR|DATA]",
			"SIZE:   [=|>|<|!][UINT]",
			"DATA:   [BEG|END|CON|EQU|REG]:[C-LIKE ESCAPE SEQUENCE|REGULAR EXPRESSION]",
			"ACTION: [DROP|FWRD|ALTR]",
			"CHANGE: [C-LIKE ESCAPE SEQUENCE] ...",
			NULL
		};

		char *omit[] = {
			"All arguments can be omitted with the keyword ANY",
			NULL
		};
		
		char *data[] = {
			"BEG: data must begin with the specified C-like sequence",
			"END: data must end with the specified C-like sequence",
			"CON: data must contain the specified C-like sequence",
			"EQU: data must equal the specified C-like sequence",
			"REG: data must match the specified regular expression",
			NULL
		};
			
		char *acts[] = {
			"FWRD: matched frames will be forwarded",
			"DROP: matched frames will be dropped",
			"ALTR: matched frames will be altered with the specified changes then forwarded",
			NULL
		};
		
		char *samp[] = {
			"CAN1 >130 DATA ANY END:\"\\x44\\x45\" ALTR \"\\x42\"",
			"ANY ANY DATA ANY REG:\"^(\\x44).*(\\x45)$\" ALTR \"\\x34\" \"\\x35\"",
			"ANY ANY RTR ANY ANY DROP",
		};
		
		char *desc[] = {
			"forward CAN1 frames with an ID higher than 130 when they end with 0x44,0x45 after replacing these 2 bytes by the only byte 0x42",
			"forward ANY frames when they start and end respectively with 0x44 and 0x45 after replacing these 2 bytes respectively with 0x34 and 0x35",
			"drop all RTR frame",
		};
		
		char **explain[] = {args, omit, data, acts};
		char *help[] = {"help", argv[0]};
		char **p;
		char *s;
		int i;
		
		canspy_cmd_help(sizeof(help) / sizeof(*help), help);
		
		for(i = 0; i < sizeof(explain) /sizeof(*explain); i++){
			
			CANSPY_UART_STATIC_LINE("-");
			p = explain[i];
			while((s = *p++) != NULL)
				canspy_print_line(CANSPY_HANDLE_UART, s, 0);
		}
		
		for(i = 0; i < sizeof(samp) /sizeof(*samp); i++){
			
			CANSPY_UART_STATIC_LINE("-");
			canspy_print_format_line(CANSPY_HANDLE_UART, "Example: %s", desc[i]);
			canspy_print_format_line(CANSPY_HANDLE_UART, "%s add %s", argv[0], samp[i]);
		}
	}
	else if(argc == 2 && STRNICONSTCMP("list", argv[1]) == 0){
		
		while((cur_rul = canspy_filter_walk(&fil_can, cur_rul)) != NULL){
			
			canspy_filter_print(CANSPY_HANDLE_UART, cur_rul);
			CANSPY_UART_BREAK();
		}
	}
	else if(argc > 7 && STRNICONSTCMP("add", argv[1]) == 0){
		
		if(canspy_filter_register(&fil_can, &cur_rul, argc - 2, argv + 2) == EXIT_SUCCESS)
			canspy_print_format_line(CANSPY_HANDLE_UART, "rule %i successfully added", cur_rul->rul_id + 1);
		else
			CANSPY_UART_STATIC_LINE("invalid syntax, see help and use cases");
	}
	else if(argc == 3 && STRNICONSTCMP("del", argv[1]) == 0){

		rul_id = strtol(argv[2], NULL, 0);
		
		if(rul_id == 0)
			CANSPY_UART_STATIC_LINE("invalid rule ID");
		else{
			
			if(canspy_filter_remove(&fil_can, rul_id - 1) == EXIT_SUCCESS)
				canspy_print_format_line(CANSPY_HANDLE_UART, "rule %i successfully deleted", rul_id);
			else
				canspy_print_format_line(CANSPY_HANDLE_UART, "rule %i does not exist", rul_id);
		}
	}
	else
		CANSPY_UART_STATIC_LINE("invalid syntax, see help");
}

/**
  * @brief  The inject command.
  * @param  argc/argv: the standard input for commands.
  * @retval None
  */
void canspy_cmd_inject(int argc, char **argv){
	
	uint8_t can_if;
	uint32_t can_val;
	uint32_t can_id;
	uint8_t can_buf[8];
	
	if(argc < 3 + 1){
		
		CANSPY_UART_STATIC_LINE("missing argument");
		return;
	}
	else if(argc > 3 + 8){
		
		CANSPY_UART_STATIC_LINE("extended data field not supported");
		return;
	}
	
	/* Get CAN interface */
	if(strcasecmp(CANSPY_FILTER_CAN_String[CAN_IF1], argv[1]) == 0)
		can_if = CAN1_IF;
	else if(strcasecmp(CANSPY_FILTER_CAN_String[CAN_IF2], argv[1]) == 0)
		can_if = CAN2_IF;
	else if(strcasecmp(CANSPY_FILTER_CAN_String[CAN_ALL], argv[1]) == 0)
		can_if = CAN_ALL;
	else{
		
		CANSPY_UART_STATIC_LINE("invalid CAN interface");
		return;
	}
	
	/* Get CAN identifier */
	if((can_id = (uint32_t)strtol(argv[2], NULL, 0)) == 0 || can_id > 0x1FFFFFFF){
		
		CANSPY_UART_STATIC_LINE("invalid id");
		return;
	}
	else if(can_id > 0x7FF){
		
		can_id |= SOCKETCAN_EFF;
	}

	/* Get CAN message */
	for(int i = 3; i < argc; i++){

		if((can_val = (uint32_t)strtol(argv[i], NULL, 10)) > 255){
			
			CANSPY_UART_STATIC_LINE("invalid data");
			return;
		}
		else{
			can_buf[i - 3] = can_val;
		}
	}
	
	/* Copy data now everything checks out */
	CANSPY_INJ_UART_If = can_if;
	CANSPY_INJ_UART_Sock.id = can_id;
	CANSPY_INJ_UART_Sock.dlc = argc - 3;
	memcpy(CANSPY_INJ_UART_Sock.data, can_buf, CANSPY_INJ_UART_Sock.dlc);
	
	/* Signaling the injection request */
	canspy_can_rewrap(&CANSPY_INJ_UART_Sock);
	CANSPY_SCHED_SIGNAL(INJ_UART);
}

/**
  * @brief  The ls command.
  * @param  argc/argv: the standard input for commands.
  * @retval None
  */
void canspy_cmd_ls(int argc, char **argv){
	
	int i, len;
	char **entry_list;
	char *entry_default[] = {"."};
	static DIR cur_dir;
	static FILINFO cur_file;
	static char lfname[_MAX_LFN + 1];
	
	if(argc > 1){
		
		len = argc - 1;
		entry_list = &(argv[1]);
	}
	else{
		
		len = 1;
		entry_list = entry_default;
	}
	
	cur_file.lfname = lfname;
	cur_file.lfsize = sizeof(lfname);

	for(i = 0; i < len; i++){
		
		if(f_opendir(&cur_dir, entry_list[i]) != FR_OK){
			
			canspy_print_format_line(CANSPY_HANDLE_UART, "%s: no such file or directory", entry_list[i]);
		}
		else{
			
			if(len > 1)
				canspy_print_format_line(CANSPY_HANDLE_UART, "%s:", entry_list[i]);
		
			while(f_readdir(&cur_dir, &cur_file) == FR_OK && cur_dir.sect != 0){
				
				if((cur_file.fattrib & AM_DIR) == AM_DIR)
					canspy_print_format_line(CANSPY_HANDLE_UART, "%s %s %s", "<DIR>", cur_file.fname, cur_file.lfname[0] != '\0' ? cur_file.lfname : "");
			}
			
			f_closedir(&cur_dir);
			f_opendir(&cur_dir, entry_list[i]);
			
			while(f_readdir(&cur_dir, &cur_file) == FR_OK && cur_dir.sect != 0){
				
				if((cur_file.fattrib & AM_DIR) != AM_DIR)
					canspy_print_format_line(CANSPY_HANDLE_UART, "%s %s %i %s", "     ", cur_file.fname, cur_file.fsize, cur_file.lfname[0] != '\0' ? cur_file.lfname : "");
			}
			
			f_closedir(&cur_dir);
		}
	}
}

/**
  * @brief  The rm command.
  * @param  argc/argv: the standard input for commands.
  * @retval None
  */
void canspy_cmd_rm(int argc, char **argv){
	
	int i;
	
	if(argc < 2){
		
		CANSPY_UART_STATIC_LINE("missing argument(s)");
		return;
	}
	
	for(i = 1; i < argc; i++){
		
		if(f_unlink(argv[i]) != FR_OK)
			canspy_print_format_line(CANSPY_HANDLE_UART, "%s: cannot remove entry", argv[i]);
	}
}

/**
  * @brief  The mv command.
  * @param  argc/argv: the standard input for commands.
  * @retval None
  */
void canspy_cmd_mv(int argc, char **argv){
	
	if(argc != 3){
		
		CANSPY_UART_STATIC_LINE("invalid syntax, see help");
		return;
	}
	
	if(f_rename(argv[1], argv[2]) != FR_OK)
		canspy_print_format_line(CANSPY_HANDLE_UART, "%s: cannot rename entry", argv[1]);
}

/**
  * @brief  The cd command.
  * @param  argc/argv: the standard input for commands.
  * @retval None
  */
void canspy_cmd_cd(int argc, char **argv){
	
	if(argc != 2){
		
		CANSPY_UART_STATIC_LINE("invalid syntax, see help");
		return;
	}
	
	if(f_chdir(argv[1]) != FR_OK)
		canspy_print_format_line(CANSPY_HANDLE_UART, "%s: no such file or directory", argv[1]);
}

/**
  * @brief  The pwd command.
  * @param  argc/argv: the standard input for commands.
  * @retval None
  */
void canspy_cmd_pwd(int argc, char **argv){
	
	f_getcwd(CANSPY_SDCARD_Path, sizeof(CANSPY_SDCARD_Path));
	
	canspy_print_format_line(CANSPY_HANDLE_UART, "%s", CANSPY_SDCARD_Path);
}

/**
  * @brief  The cat command.
  * @param  argc/argv: the standard input for commands.
  * @retval None
  */
void canspy_cmd_cat(int argc, char **argv){
	
	int i, j;
	UINT read;
	char *buf;
	
	if(argc < 2){
		
		CANSPY_UART_STATIC_LINE("missing arguments");
		return;
	}
	
	if((buf = malloc(sizeof(CANSPY_UART_Tx_Buf) / 10)) == NULL){
		
		CANSPY_DEBUG_ERROR_0(FATAL, CANSPY_MEMORY_ERROR_Alloc);
		return;
	}
	
	canspy_sdcard_init(&CANSPY_CMD_FIL_Shared);
	
	for(i = 1; i < argc; i++){
		
		if(canspy_sdcard_open(&CANSPY_CMD_FIL_Shared, argv[i], FA_OPEN_EXISTING | FA_READ) == EXIT_SUCCESS){
		
			while(canspy_sdcard_read(&CANSPY_CMD_FIL_Shared, buf, (sizeof(CANSPY_UART_Tx_Buf) / 10), &read) == EXIT_SUCCESS)
				for(j = 0; j < read; j++)
					canspy_print_format(CANSPY_HANDLE_UART, "%c", buf[j]);
			
			canspy_sdcard_close(&CANSPY_CMD_FIL_Shared);
		}
		else
			canspy_print_format_line(CANSPY_HANDLE_UART, "%s: no such file or directory", argv[i]);
	}
	
	free(buf);
}

/**
  * @brief  The xxd command.
  * @param  argc/argv: the standard input for commands.
  * @retval None
  */
void canspy_cmd_xxd(int argc, char **argv){
	
	int i, j;
	UINT read;
	int word_size = 2;
	int line_size = 16;
	char buf[line_size];
	
	if(argc != 2){
		
		CANSPY_UART_STATIC_LINE("invalid number of arguments");
		return;
	}
	
	canspy_sdcard_init(&CANSPY_CMD_FIL_Shared);
		
	if(canspy_sdcard_open(&CANSPY_CMD_FIL_Shared, argv[1], FA_OPEN_EXISTING | FA_READ) != EXIT_SUCCESS){
		
		canspy_print_format_line(CANSPY_HANDLE_UART, "%s: no such file or directory", argv[1]);
		return;
	}
	
	i = 0;

	while(canspy_sdcard_read(&CANSPY_CMD_FIL_Shared, buf, line_size, &read) == EXIT_SUCCESS){
		
		canspy_print_format(CANSPY_HANDLE_UART, "%07x: ", i);
		i += line_size;
		
		for(j = 0; j < read; j++){
			
			if((j != 0) && (j % word_size == 0))
					CANSPY_UART_STATIC_STRING(" ");
			
			canspy_print_format(CANSPY_HANDLE_UART, "%02x", buf[j]);
		}
		
		for(j = read; j < line_size; j++){
			
			if((j != 0) && (j % word_size == 0))
					CANSPY_UART_STATIC_STRING(" ");
			
			CANSPY_UART_STATIC_STRING("  ");
		}
		
		CANSPY_UART_STATIC_STRING(" ");
		
		for(j = 0; j < read; j++)
			canspy_print_printable(CANSPY_HANDLE_UART, buf[j]);
		
		CANSPY_UART_BREAK();
	}
	
	canspy_sdcard_close(&CANSPY_CMD_FIL_Shared);
}

/**
  * @brief  The mkdir command.
  * @param  argc/argv: the standard input for commands.
  * @retval None
  */
void canspy_cmd_mkdir(int argc, char **argv){
	
	int i;
	
	if(argc < 2){
		
		CANSPY_UART_STATIC_LINE("missing arguments");
		return;
	}
	
	for(i = 1; i < argc; i++){
		
		if(f_mkdir(argv[i]) != FR_OK)
			canspy_print_format_line(CANSPY_HANDLE_UART, "%s: cannot create entry", argv[i]);
	}
}

/**
  * @brief  The rmdir command.
  * @param  argc/argv: the standard input for commands.
  * @retval None
  */
void canspy_cmd_rmdir(int argc, char **argv){
	
	int i;
	FRESULT res;
	DIR *cur_dir;
	FILINFO *cur_file;
	
	/* Recursive trick to avoid erasing CANSPY_SDCARD_Path during recursion */
	if(argc == 0 && argv == NULL){
			
		if((cur_dir = malloc(sizeof(*cur_dir))) == NULL){
		
			CANSPY_DEBUG_ERROR_0(FATAL, CANSPY_MEMORY_ERROR_Alloc);
			return;
		}
		
		if((cur_file = malloc(sizeof(*cur_file))) == NULL){

			CANSPY_DEBUG_ERROR_0(FATAL, CANSPY_MEMORY_ERROR_Alloc);
			free(cur_dir);
			return;
		}

		if(f_opendir(cur_dir, ".") == FR_OK){

			while(f_readdir(cur_dir, cur_file) == FR_OK && cur_dir->sect != 0){
				
				if((STRNICONSTCMP(".", cur_file->fname) != 0) && (STRNICONSTCMP("..", cur_file->fname) != 0)){
				
					if((cur_file->fattrib & AM_DIR) == AM_DIR){
						
						f_chdir(cur_file->fname);
						canspy_cmd_rmdir(0, NULL);
						f_chdir("..");
					}

					if((res = f_unlink(cur_file->fname)) != FR_OK){

						canspy_print_format_line(CANSPY_HANDLE_UART, "%s: cannot remove entry (error %i)", cur_file->fname, res);
					}
				}
			}
			
			f_closedir(cur_dir);
		}
		
		free(cur_file);
		free(cur_dir);
	}
	else if(argc < 2){
		
		CANSPY_UART_STATIC_LINE("missing arguments");
		return;
	}
	else{
	
		f_getcwd(CANSPY_SDCARD_Path, sizeof(CANSPY_SDCARD_Path));
		
		for(i = 1; i < argc; i++){
			
			if(f_chdir(argv[i]) == FR_OK){
				
				canspy_cmd_rmdir(0, NULL);
				f_chdir(CANSPY_SDCARD_Path);
				
				if((res = f_unlink(argv[i])) != FR_OK)
					canspy_print_format_line(CANSPY_HANDLE_UART, "%s: cannot remove entry (error %i)", argv[i], res);
			}
			else
				canspy_print_format_line(CANSPY_HANDLE_UART, "%s: no such directory", argv[i]);
		}
	}
}

/**
  * @brief  The pcap command.
  * @param  argc/argv: the standard input for commands.
  * @retval None
  */
void canspy_cmd_pcap(int argc, char **argv){

	uint8_t can_if;
	static CanRxMsgTypeDef RxMess;
	static SOCKETCAN PCAPSocket;
	static CANSPY_PCAP_HEADER_RECORD cur_header;
	
	if(argc != 2){
		
		CANSPY_UART_STATIC_LINE("invalid number of arguments");
		return;
	}
	
	canspy_sdcard_init(&CANSPY_CMD_FIL_Shared);
		
	if(canspy_sdcard_open(&CANSPY_CMD_FIL_Shared, argv[1], FA_OPEN_EXISTING | FA_READ) != EXIT_SUCCESS){
		
		canspy_print_format_line(CANSPY_HANDLE_UART, "%s: no such file or directory", argv[1]);
		return;
	}
	
	if(canspy_pcap_check(&CANSPY_CMD_FIL_Shared) != EXIT_SUCCESS){
		
		canspy_print_format_line(CANSPY_HANDLE_UART, "%s: not a valid PCAP file", argv[1]);
		canspy_sdcard_close(&CANSPY_CMD_FIL_Shared);
		return;
	}

	while(canspy_pcap_walk(&CANSPY_CMD_FIL_Shared, &PCAPSocket, &cur_header, &can_if) == EXIT_SUCCESS){
		
		canspy_can_unwrap(&RxMess, &PCAPSocket, true);
		canspy_debug_frame(CANSPY_HANDLE_UART, cur_header.ts_sec, cur_header.ts_usec, can_if, &RxMess);
	}
	
	canspy_sdcard_close(&CANSPY_CMD_FIL_Shared);
}
