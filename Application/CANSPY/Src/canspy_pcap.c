/**
  ******************************************************************************
  * @file    canspy_pcap.c
  * @author  Arnaud Lebrun <a-lebrun@live.fr>
  * @author  Jonathan-Christofer Demay <jcdemay@gmail.com>
  * @version V1.0
  * @brief   PCAP module extension.
  *          This file provides all the functions needed to manage PCAP files:
  *           + File header
  *
  ******************************************************************************
  * COPYRIGHT(c) 2016 Arnaud Lebrun and Jonathan-Christofer Demay
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of the copyright holder nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  ******************************************************************************
  */ 

#include "canspy_pcap.h"
#include "canspy_eth.h"
#include "canspy_sdcard.h"

/** @brief  The global PCAP header.
  */
CANSPY_PCAP_HEADER_GLOBAL CANSPY_PCAP_FILE_Header = {0xa1b2c3d4, 0x2, 0x4, 0x0, 0x0, 0x7fff, 0x1};

/**
  * @brief  Check if a file is a valid PCAP.
  * @param  fp: a pointer to an open and valid file structure.
  * @retval Exit Status
  */  
int canspy_pcap_check(CANSPY_SDCARD_FIL *fp){
	
	UINT read;
	static CANSPY_PCAP_HEADER_GLOBAL fil_header;
	
	if(canspy_sdcard_read(fp, &fil_header, sizeof(fil_header), &read) != EXIT_SUCCESS || read != sizeof(fil_header)
	|| memcmp(&fil_header, &CANSPY_PCAP_FILE_Header, sizeof(fil_header)) != 0)
			return EXIT_FAILURE;
	else
		return EXIT_SUCCESS;
}

/**
  * @brief  Walk through all CAN frames stored in a valid PCAP file.
  * @param  fp: a pointer to a buffer or to an open and valid file structure.
  * @param  buffered: specifies whether it relies on buffered reads or not.
  * @param  cap_sock: a pointer to store the current CAN frame.
  * @param  rec_header: a pointer to store the current record header (optional).
  * @param  can_if: a pointer to store the current frame source (optional).
  * @retval Exit Status
  */  
int canspy_pcap_walk(CANSPY_SDCARD_FIL *fp, SOCKETCAN *cap_sock, CANSPY_PCAP_HEADER_RECORD *rec_header, uint8_t *can_if){

	UINT read;
	uint16_t can_type;
	static uint8_t src_mac[6];
	static CANSPY_PCAP_HEADER_RECORD new_header;
	
	while(canspy_sdcard_read(fp, &new_header, sizeof(new_header), &read) == EXIT_SUCCESS && read == sizeof(new_header)){
	
		if(canspy_sdcard_read(fp, ETH_Tx_Buf[0], new_header.incl_len, &read) != EXIT_SUCCESS || read != new_header.incl_len)
			return EXIT_FAILURE;
		
		can_type = canspy_eth_recv(ETH_Tx_Buf[0], cap_sock, sizeof(*cap_sock), NULL, src_mac);
		
		if(can_type == CANSPY_ETH_Type[CAN1_RX] || can_type == CANSPY_ETH_Type[CAN2_RX]){

			if(can_if != NULL){

				if(memcmp(src_mac, CANSPY_ETH_Mac[CAN1_RX], sizeof(src_mac)) == 0)
					*can_if = CAN1_IF;
				else if(memcmp(src_mac, CANSPY_ETH_Mac[CAN2_RX], sizeof(src_mac)) == 0)
					*can_if = CAN2_IF;
				else if(src_mac[0] == 0x00 && src_mac[1] == 0x80 && src_mac[2] == 0xE1){
					
					/* We assume here that the file comes from another CANSPY */
					if(src_mac[5] == CAN1_IF)
						*can_if = CAN1_IF;
					else if(src_mac[5] == CAN2_IF)
						*can_if = CAN2_IF;
					else
						*can_if = 0;
				}
				else
					*can_if = 0;
			}
			
			if(rec_header != NULL)
				*rec_header = new_header;
			
			return EXIT_SUCCESS;
		}
	}
	
	return EXIT_FAILURE;
}

/**
  * @brief  Write a CAN frame in a valid PCAP file.
  * @param  fp: a pointer to a buffer or to an open and valid file structure.
  * @param  buffered: specifies whether it relies on buffered writes or not.
  * @param  cap_sock: a pointer to the CAN frame to write.
  * @param  rec_header: a pointer to the record header to write.
  * @param  can_if: the source of the CAN frame.
  * @retval Exit Status or the number of buffered bytes.
  */
int canspy_pcap_write(CANSPY_SDCARD_FIL *fp, SOCKETCAN *pcan_frame, CANSPY_PCAP_HEADER_RECORD *rec_header, uint8_t can_if){

	canspy_eth_send(ETH_Tx_Buf[0], pcan_frame, sizeof(*pcan_frame), CANSPY_ETH_Mac[can_if], CANSPY_ETH_Mac[ETH_BROADCAST], CANSPY_ETH_Type[can_if], true);
	
	rec_header->ts_usec = HAL_GetTick();
	rec_header->ts_sec = rec_header->ts_usec / 1000;
	rec_header->ts_usec = rec_header->ts_usec - (rec_header->ts_sec * 1000);
	rec_header->orig_len = sizeof(*pcan_frame) - sizeof(pcan_frame->data) + pcan_frame->dlc + CANSPY_ETH_HEADER_SIZE;
	rec_header->incl_len  = rec_header->orig_len;

	return canspy_sdcard_write(fp, rec_header, sizeof(*rec_header), false) | canspy_sdcard_write(fp, (uint8_t *)ETH_Tx_Buf, rec_header->incl_len, false);
}
