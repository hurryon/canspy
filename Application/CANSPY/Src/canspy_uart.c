/**
  ******************************************************************************
  * @file    canspy_uart.c
  * @author  Arnaud Lebrun <a-lebrun@live.fr>
  * @author  Jonathan-Christofer Demay <jcdemay@gmail.com>
  * @version V1.0
  * @brief   UART module core.
  *          This file provides all the functions needed by UART services:
  *           + Traffic monitoring
  *           + Event viewing
  *           + Interactive shell
  *
  ******************************************************************************
  * COPYRIGHT(c) 2016 Arnaud Lebrun and Jonathan-Christofer Demay
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of the copyright holder nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  ******************************************************************************
  */ 

#include "canspy_uart.h"
#include "canspy_shell.h"
#include "canspy_cmd.h"
#include "canspy_print.h"
#include "canspy_debug.h"

/** @brief  The default error message for UART sequences.
  */
char *CANSPY_UART_ERROR_Sequence = "Escape sequence not implemented";

/** @brief  A pointer to the print service structure.
  */
int CANSPY_HANDLE_UART_Svc_id = -1;

/** @brief  Buffer and iterator for UART TX.
  */
uint8_t CANSPY_UART_Tx_Buf[UART_LINE_TX_BUF_SIZE];
int CANSPY_UART_Tx_Iter = 0;

/** @brief  Buffer and iterator for UART RX.
  */
uint8_t CANSPY_UART_Rx_Buf[UART_LINE_RX_BUF_SIZE];
int CANSPY_UART_Rx_Iter = 0;

/** @brief  The three-character sequence (starts like two-character sequence)
  */
uint8_t ESC_ARROW[] = {CHAR_ESC, CHAR_ARROW};

/** @brief  The delete sequence (in fact it is backspace)
  */
uint8_t DEL_SEQ[] = {CHAR_BS, CHAR_SPACE, CHAR_BS};

/**
  * @brief  The UART device handler.
  * @param  call: the call to process.
  * @param  opt: an optionnal parameter.
  * @retval Exit Status
  */
int canspy_uart_device(CANSPY_DEVICE_CALL call, CANSPY_OPTION_CALL *opt){
	
	if(call == DEV_INIT){
		
		CANSPY_DEVICE_OPTION("uart_dbg", CANSPY_OPTION_TYPES_Range[OPTION_DEBUG], CANSPY_OPTION_TYPES_Length[OPTION_DEBUG],
			&CANSPY_DEBUG_UART_Print, "Specifies the level of debug messages that should be printed on the UART console", INSTANT);
	}
	else if(call == DEV_START){
		
		if(MX_USART6_UART_Init() != HAL_OK){
			
			CANSPY_DEBUG_ERROR_0(ERROR, "Failed to init UART HAL");
			return EXIT_FAILURE;
		}
		
		while(HAL_UART_Receive_IT(&huart6, UART_Rx_Buf, sizeof(UART_Rx_Buf)) != HAL_OK);
		
		if(HAL_UART_Transmit(&huart6, (uint8_t *)CANSPY_PRINT_BREAK, sizeof(CANSPY_PRINT_BREAK), UART_TX_TIMEOUT) != HAL_OK){
			
			CANSPY_DEBUG_ERROR_0(ERROR, "Failed to transmit on UART");
			return EXIT_FAILURE;
		}
	}
	else if(call == DEV_STOP){
		
		if(HAL_UART_DeInit(&huart6) != HAL_OK){
			
			CANSPY_DEBUG_ERROR_0(ERROR, "Failed to deinit UART HAL");
			return EXIT_FAILURE;
		}
	}
	else if(call == DEV_GET){

		if(CANSPY_OPTION_CMP(&CANSPY_DEBUG_UART_Print))
			CANSPY_OPTION_GET(debug, &CANSPY_DEBUG_UART_Print);
		else{

			CANSPY_DEBUG_ERROR_1(WARN, CANSPY_OPTION_ERROR_Unknown, CANSPY_OPTION_VAR);
			return EXIT_FAILURE;
		}
	}
	else if(call == DEV_SET){
		
		if(CANSPY_OPTION_CMP(&CANSPY_DEBUG_UART_Print))
			CANSPY_OPTION_SET(debug, &CANSPY_DEBUG_UART_Print);
		else{

			CANSPY_DEBUG_ERROR_1(WARN, CANSPY_OPTION_ERROR_Unknown, CANSPY_OPTION_VAR);
			return EXIT_FAILURE;
		}
	}
	else{
		
		CANSPY_DEBUG_ERROR_1(WARN, CANSPY_DEVICE_ERROR_Unknown, call);
		return EXIT_FAILURE;
	}
	
	return EXIT_SUCCESS;
}

/**
  * @brief  Defined as UART_Rx_Handler.
  * @param  rx_char: the received character.
  * @retval Handled code
  */
int canspy_uart_handler(uint8_t rx_char){
	
	bool b_echo = false;
	static bool b_escape = false;
	static bool b_arrow = false;
	static bool b_drop_next = false;
	
	/* Setting the Rx flag */
	CANSPY_SCHED_SIGNAL(UART_RX);
	
	/* Dealing with escape sequences */
	if(rx_char == CHAR_ESC){
		
		/* Starting two-character escape sequence */
		b_escape = true;
	}
	else if(b_escape){
		
		b_escape = false;
		
		if(rx_char == CHAR_ARROW){
			
			/* Starting three-character escape sequence */
			b_arrow = true;
		}
		else{
			
			/* Set the next character to be dropped and raise warning */
			b_drop_next = true;
			CANSPY_DEBUG_ERROR_0(INFO, CANSPY_UART_ERROR_Sequence);
		}
	}
	else if(b_arrow){
		
		b_arrow = false;
		
		if(rx_char == CHAR_LEFT){
			
			/* Check if iterator is already at the beginning */
			if(CANSPY_UART_Rx_Iter > 0){
				
				b_echo = true;
				CANSPY_UART_Rx_Iter--;
			}
		}
		else if(rx_char == CHAR_RIGHT){
		
			/* Check if iterator is already at the end */
			if(CANSPY_UART_Rx_Iter < sizeof(CANSPY_UART_Rx_Buf) - 1){
				
				b_echo = true;
				CANSPY_UART_Rx_Iter++;
			}
		}
		else if(rx_char == CHAR_UP || rx_char == CHAR_DOWN /*|| rx_char == CHAR_PAGE_UP || rx_char == CHAR_PAGE_DOWN*/){

			/* Triggering the escape sequence */
			CANSPY_UART_Rx_Buf[0] = ESC_ARROW[0];
			CANSPY_UART_Rx_Buf[1] = ESC_ARROW[1];
			CANSPY_UART_Rx_Buf[2] = rx_char;
			CANSPY_SCHED_SIGNAL(UART_BREAK);
			
			/*if(rx_char == CHAR_PAGE_UP || rx_char == CHAR_PAGE_DOWN)
				b_drop_next = true;*/
		}
		else{
			
			/* Set the next character to be dropped and raise warning */
			b_drop_next = true;
			CANSPY_DEBUG_ERROR_0(INFO, CANSPY_UART_ERROR_Sequence);
		}

		/* Preparing to echo the received character */
		if(b_echo){

			while(HAL_UART_Transmit(&huart6, ESC_ARROW, sizeof(ESC_ARROW), UART_TX_TIMEOUT) != HAL_OK);
		}
	}
	else if(rx_char == CHAR_DEL){
		
		/* Clearing the previous character if there is one */
		if(CANSPY_UART_Rx_Iter > 0){
			
			while(HAL_UART_Transmit(&huart6, DEL_SEQ, sizeof(DEL_SEQ), UART_TX_TIMEOUT) != HAL_OK);
			CANSPY_UART_Rx_Iter--;
		}
	}
	else{
		
		/* Checking if the character is supposed to be processed */
		if(b_drop_next == false){
			
			/* Adding the received character to the shell buffer and make it echoable */
			CANSPY_UART_Rx_Buf[CANSPY_UART_Rx_Iter] = rx_char;
			CANSPY_UART_Rx_Iter++;
			b_echo = true;

			/* Checking if a line break was received */
			if((CANSPY_UART_Rx_Iter >= sizeof(CANSPY_UART_Rx_Buf)) || (CANSPY_UART_Rx_Buf[CANSPY_UART_Rx_Iter - 1] == CHAR_LF)){
			
				if(CANSPY_UART_Rx_Buf[CANSPY_UART_Rx_Iter - 2] == CHAR_CR)
					CANSPY_UART_Rx_Buf[CANSPY_UART_Rx_Iter - 2] = '\0';
				
				/* Triggering the line break */
				CANSPY_UART_Rx_Buf[CANSPY_UART_Rx_Iter - 1] = '\0';
				CANSPY_UART_Rx_Iter = 0;
				CANSPY_SCHED_SIGNAL(UART_BREAK);
			}
		}
		else{
			
			/* The next character will now be processed */
			b_drop_next = false;
		}
	}

	if(b_echo)
		return 0;
	else
		return !0;
}

/**
  * @brief  Add a buffer to the print queue.
  * @param  buffer: the buffer to queue.
  * @param  length: the length of the buffer.
  * @retval None
  */
void canspy_uart_queue(uint8_t *buffer, int length){
	
	static int len_right, len_left;
	static int off_right, off_left;
	static int print_space;
	
	if(!CANSPY_SCHED_Running || CANSPY_HANDLE_UART_Svc_id == -1 || !CANSPY_DEVICE_POINTER(CANSPY_UART)->svc_list[CANSPY_HANDLE_UART_Svc_id].started){ 
		
		while(HAL_UART_Transmit(&huart6, (uint8_t *)buffer, length, UART_TX_TIMEOUT) != HAL_OK);
		return;
	}
	
	print_space = sizeof(CANSPY_UART_Tx_Buf) - CANSPY_SCHED_Flags[UART_TX];
	
	if(length > print_space)
		length = print_space;
	
	if(length == 0)
		return;
	
	if(CANSPY_UART_Tx_Iter + CANSPY_SCHED_Flags[UART_TX] >= sizeof(CANSPY_UART_Tx_Buf)){
		
		off_right = 0;
		off_left = CANSPY_SCHED_Flags[UART_TX] - (sizeof(CANSPY_UART_Tx_Buf) - 1 - CANSPY_UART_Tx_Iter);
		
		len_right = 0;

		if(off_left + length >= CANSPY_UART_Tx_Iter)
			len_left = CANSPY_UART_Tx_Iter - 1 - off_left;
		else
			len_left = length;
	}
	else{
		
		off_right = CANSPY_UART_Tx_Iter + CANSPY_SCHED_Flags[UART_TX];
		off_left = 0;

		if(off_right + len_right >= sizeof(CANSPY_UART_Tx_Buf) - 1)
			len_right = sizeof(CANSPY_UART_Tx_Buf) - 1 - off_right;
		else
			len_right = length;
		
		len_left = length - len_right;
		
		if(CANSPY_UART_Tx_Iter > 0)
			if(len_left >= CANSPY_UART_Tx_Iter)
				len_left = CANSPY_UART_Tx_Iter - 1;
	}
	
	if(len_right != 0)
		memcpy(CANSPY_UART_Tx_Buf + off_right, buffer, len_right);
	
	memcpy(CANSPY_UART_Tx_Buf + off_left, buffer + len_right, len_left);
	
	CANSPY_SCHED_SIGNAL2(UART_TX, len_right + len_left);
}

/**
  * @brief  The print service.
  * @param  flag: the flag to process.
  * @param  opt: an optionnal parameter.
  * @retval Exit Status
  */
int canspy_uart_print(CANSPY_SERVICE_FLAG flag, CANSPY_OPTION_CALL *opt){
		
	if(flag == SVC_INIT){
		
		CANSPY_HANDLE_UART_Svc_id = opt->svc_ptr->svc_id;
	}
	else if(flag == UART_TX){
		
		if(HAL_UART_Transmit_IT(&huart6, CANSPY_UART_Tx_Buf + CANSPY_UART_Tx_Iter, 1) != HAL_OK){
			
			/* Try again next round */
			CANSPY_SCHED_SIGNAL(UART_TX);
		}
		else{
		
			if(++CANSPY_UART_Tx_Iter >= sizeof(CANSPY_UART_Tx_Buf) - 1)
				CANSPY_UART_Tx_Iter = 0;
		}
	}
	
	return EXIT_SUCCESS;
}

/**
  * @brief  The monitor service.
  * @param  flag: the flag to process.
  * @param  opt: an optionnal parameter.
  * @retval Exit Status
  */
int canspy_uart_monitor(CANSPY_SERVICE_FLAG flag, CANSPY_OPTION_CALL *opt){

	uint32_t frame_stamp;
	uint32_t frame_milli;
		
	if(flag == SVC_START){
			
		CANSPY_UART_STATIC_LINE("Press ENTER to stop monitoring CAN traffic");
	}
	else if(flag == SVC_STOP){

		CANSPY_UART_STATIC_LINE("Stopped monitoring CAN traffic, press ENTER again");
	}
	else if(flag == UART_BREAK){

		/* Stop monitoring when a line break is received */
		CANSPY_SERVICE_NOMORE();
	}
	else if(flag == CAN1_RX || flag == CAN2_RX){

		/* Process flag using the debug frame function */
		frame_milli = HAL_GetTick();
		frame_stamp = frame_milli / 1000;
		frame_milli = frame_milli - (frame_stamp * 1000);
		canspy_debug_frame(CANSPY_HANDLE_UART, frame_stamp, frame_milli, flag, canspy_sched_buffer(flag));
	}
	
	return EXIT_SUCCESS;
}

/**
  * @brief  The viewing service.
  * @param  flag: the flag to process.
  * @param  opt: an optionnal parameter.
  * @retval Exit Status
  */
int canspy_uart_viewing(CANSPY_SERVICE_FLAG flag, CANSPY_OPTION_CALL *opt){

	if(flag == SVC_START){
			
		CANSPY_UART_STATIC_LINE("Press ENTER to stop viewing events");
	}
	else if(flag == SVC_STOP){

		CANSPY_UART_STATIC_LINE("Stopped event viewing, press ENTER again");
	}
	else if(flag == UART_BREAK){

		/* Stop viewing events when a line break is received */
		CANSPY_SERVICE_NOMORE();
	}
	else{
		
		/* Process flag using the debug event function */
		canspy_debug_event(flag, CANSPY_HANDLE_UART);
	}
	
	return EXIT_SUCCESS;
}

/**
  * @brief  The shell service.
  * @param  flag: the flag to process.
  * @param  opt: an optionnal parameter.
  * @retval Exit Status
  */
int canspy_uart_shell(CANSPY_SERVICE_FLAG flag, CANSPY_OPTION_CALL *opt){
		
	if(flag == SVC_INIT){

		/* Initializing the shell history */
		for(int i = 0; i < CANSPY_SHELL_HISTORY; i++)
			CANSPY_SHELL_HISTORY_Buf[i][0] = '\0';

		/* Shell commands are registered here */
		canspy_shell_register(canspy_cmd_help, "help", "display help for commands", "<COMMAND>", CANSPY_NODEVICE);
		canspy_shell_register(canspy_cmd_stats, "stats", "display information about the platform", "[list|reset] [CAN1|CAN2|ALL]", CANSPY_NODEVICE);
		canspy_shell_register(canspy_cmd_device, "device", "display or change device status", "[list|status|start|stop|pause|resume] <DEVICE>", CANSPY_NODEVICE);
		canspy_shell_register(canspy_cmd_service, "service", "display or change service status", "[list|status|start|stop] <SERVICE>", CANSPY_NODEVICE);
		canspy_shell_register(canspy_cmd_option, "option", "display or change service and device options", "[list|get|set] <NAME> [VALUE]>", CANSPY_NODEVICE);
		canspy_shell_register(canspy_cmd_filter, "filter", "display or change filtering rules", "[help|list|del|add] <[RULE] | [IF] [ID] [TYPE] [SIZE] [DATA] [ACTION] <CHANGE> ...>", CANSPY_NODEVICE);
		canspy_shell_register(canspy_cmd_inject, "inject", "inject frame on a CAN bus", "[CAN1|CAN2|ALL] [ID] [BYTE] ...", CANSPY_NODEVICE);
		canspy_shell_register(canspy_cmd_ls, "ls", "list the content of directories", "<PATH> ...", CANSPY_SDCARD);
		canspy_shell_register(canspy_cmd_rm, "rm", "remove files or empty directories", "[PATH] <PATH> ...", CANSPY_SDCARD);
		canspy_shell_register(canspy_cmd_mv, "mv", "move files or directories", "[PATH] [PATH]", CANSPY_SDCARD);
		canspy_shell_register(canspy_cmd_cd, "cd", "change the current directory", "[PATH]", CANSPY_SDCARD);
		canspy_shell_register(canspy_cmd_pwd, "pwd", "print the current/working directory", "", CANSPY_SDCARD);
		canspy_shell_register(canspy_cmd_cat, "cat", "concatenate and print files", "[PATH] <PATH> ...", CANSPY_SDCARD);
		canspy_shell_register(canspy_cmd_xxd, "xxd", "print the hexadecimal dump of a file", "[PATH]", CANSPY_SDCARD);
		canspy_shell_register(canspy_cmd_pcap, "pcap", "print a capture file (PCAP format)", "[PATH]", CANSPY_SDCARD);
		canspy_shell_register(canspy_cmd_mkdir, "mkdir", "create new directories", "[PATH] <PATH> ...", CANSPY_SDCARD);
		canspy_shell_register(canspy_cmd_rmdir, "rmdir", "remove non-empty directories", "[PATH] <PATH> ...", CANSPY_SDCARD);
	}
	else if(flag == SVC_POST){
			
		CANSPY_UART_BREAK();
		canspy_print_format_line(CANSPY_HANDLE_UART, "CANSPY interactive shell 1.0 (%s %s)", __DATE__, __TIME__);
		canspy_shell_prompt();
	}
	else if(flag == UART_BREAK){
		
		static int history_offset = -1;

		if(CANSPY_UART_Rx_Buf[0] == CHAR_ESC && CANSPY_UART_Rx_Buf[1] == CHAR_ARROW){
			
			int command_length;
			
			/* Computing the offset with the history buffer */
			if(CANSPY_UART_Rx_Buf[2] == CHAR_DOWN){
				
				if(history_offset != -1)
					history_offset++;
				else
					history_offset = CANSPY_SHELL_HISTORY_Ptr;
			}
			else if(CANSPY_UART_Rx_Buf[2] == CHAR_UP){

				if(history_offset != -1)
					history_offset--;
				else
					history_offset = CANSPY_SHELL_HISTORY_Ptr - 1;
			}
			else if(CANSPY_UART_Rx_Buf[2] == CHAR_PAGE_UP){

				history_offset = 0;
			}
			else if(CANSPY_UART_Rx_Buf[2] == CHAR_PAGE_DOWN){

				history_offset = CANSPY_SHELL_HISTORY - 1;
			}
			else{
				
				CANSPY_DEBUG_ERROR_0(INFO, CANSPY_UART_ERROR_Sequence);
				return EXIT_FAILURE;
			}
			
			/* Handling the circular aspect of the history buffer */
			if(history_offset < 0)
				history_offset = CANSPY_SHELL_HISTORY - 1;
			else if(history_offset >= CANSPY_SHELL_HISTORY)
				history_offset = 0;
			
			/* Clearing the command prompt */
			while(CANSPY_UART_Rx_Iter > 0){
					
				CANSPY_UART_Rx_Iter--;
				canspy_print_buffer(CANSPY_HANDLE_UART, DEL_SEQ, sizeof(DEL_SEQ));
			}
			
			/* Echoing and copying the command to retreive */
			if((command_length = strlen(CANSPY_SHELL_HISTORY_Buf[history_offset])) > 0){
					
				canspy_print_buffer(CANSPY_HANDLE_UART, CANSPY_SHELL_HISTORY_Buf[history_offset], command_length);
				memcpy(CANSPY_UART_Rx_Buf, CANSPY_SHELL_HISTORY_Buf[history_offset], command_length);
				CANSPY_UART_Rx_Iter = command_length;
			}
		}
		else{
			
			/* Reset shell history walking offset */
			history_offset = -1;
			
			/* Check if a command was entered */
			if(CANSPY_UART_Rx_Buf[0] != '\0'){
				
				/* Update the history buffer */
				strncpy(CANSPY_SHELL_HISTORY_Buf[CANSPY_SHELL_HISTORY_Ptr], (char *)CANSPY_UART_Rx_Buf, sizeof(CANSPY_UART_Rx_Buf));
				CANSPY_SHELL_HISTORY_Ptr++;
				
				/* Handle the circular aspect of the history buffer */
				if(CANSPY_SHELL_HISTORY_Ptr >= CANSPY_SHELL_HISTORY)
					CANSPY_SHELL_HISTORY_Ptr = 0;
				
				/* Run the received command */
				canspy_shell_execute((char *)CANSPY_UART_Rx_Buf);
			}

			/* Check if a command prompt is necessary */
			if(CANSPY_SERVICE_ACTIVE())
				canspy_shell_prompt();
		}
	}
	
	return EXIT_SUCCESS;
}
