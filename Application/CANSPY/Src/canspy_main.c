/**
  ******************************************************************************
  * @file    canspy_main.c
  * @author  Arnaud Lebrun <a-lebrun@live.fr>
  * @author  Jonathan-Christofer Demay <jcdemay@gmail.com>
  * @version V1.0
  * @brief   Main body of the application.
  *
  ******************************************************************************
  * COPYRIGHT(c) 2016 Arnaud Lebrun and Jonathan-Christofer Demay
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of the copyright holder nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  ******************************************************************************
  */ 

#include "canspy_sched.h"
#include "canspy_can.h"
#include "canspy_eth.h"
#include "canspy_sdcard.h"
#include "canspy_uart.h"
#include "canspy_debug.h"

/**
  * @brief  This function handles clock configuration.
  * @retval None
  */
void canspy_clock_config(void){

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;

  __PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 8;
  RCC_OscInitStruct.PLL.PLLN = 336;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 4;
  HAL_RCC_OscConfig(&RCC_OscInitStruct);

  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_SYSCLK|RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;
  HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5);

  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq() / 1000);
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);
}

/**
  * @brief  This function is the main procedure of the application.
  * @retval None
  */
int main(void){

  /* Resetting all devices */
  HAL_Init();

  /* Configuring the system clock */
  canspy_clock_config();

	/* Initializing GPIO */
	MX_GPIO_Init();
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_12, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_13, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_14, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_15, GPIO_PIN_RESET);
	
	/* Initializing the scheduler */
	canspy_sched_init();

  /* Registering all devices */
	canspy_device_register(CANSPY_UART, canspy_uart_device, "UART", &huart6, START);
	canspy_device_register(CANSPY_CAN1, canspy_can_device1, "CAN1", &hcan1, START);
	canspy_device_register(CANSPY_CAN2, canspy_can_device2, "CAN2", &hcan2, START);
	canspy_device_register(CANSPY_ETH, canspy_eth_device, "ETH", &heth, START);
	canspy_device_register(CANSPY_SDCARD, canspy_sdcard_device, "SDCARD", &hsd, STOP);
		
	/* Registering SDCARD services */
	canspy_service_register(CANSPY_SDCARD, canspy_sdcard_capture, "capture", "Traffic capture (PCAP format)", SHARE, SYNC, STOP);
	canspy_service_register(CANSPY_SDCARD, canspy_sdcard_logging, "logging", "Event logging", SHARE, SYNC, STOP);
	canspy_service_register(CANSPY_SDCARD, canspy_sdcard_replay, "replay", "Replay captured traffic", SHARE, ASYNC, STOP);

	/* Registering ETH services */
	canspy_service_register(CANSPY_ETH, canspy_eth_bridge, "bridge", "Bridge mode (Tapping and injecting)", EXCL, SYNC, STOP);
	canspy_service_register(CANSPY_ETH, canspy_eth_wiretap, "wiretap", "Wiretap mode (SocketCAN encapsulation)", EXCL, SYNC, START);
	canspy_service_register(CANSPY_ETH, canspy_eth_command, "command", "Command execution over Ethernet", SHARE, SYNC, START);
	
	/* Registering CAN1 and CAN2 services at the same time */
	CANSPY_SCHED_REGISTER_CAN_ALL(CANSPY_CAN, canspy_can_inject, "inject", "Message injection for CAN", SHARE, SYNC, START);
	CANSPY_SCHED_REGISTER_CAN_ALL(CANSPY_CAN, canspy_can_filter, "filter", "Traffic filtering for CAN", EXCL, SYNC, STOP);
	CANSPY_SCHED_REGISTER_CAN_ALL(CANSPY_CAN, canspy_can_forward, "forward", "Traffic forwarding for CAN", EXCL, SYNC, STOP);
		
	/* Registering UART services */
	canspy_service_register(CANSPY_UART, canspy_uart_print, "print", "Synchronous printing (optional)", SHARE, SYNC, STOP);
	canspy_service_register(CANSPY_UART, canspy_uart_monitor, "monitor", "Traffic monitoring", EXCL, SYNC, STOP);
	canspy_service_register(CANSPY_UART, canspy_uart_viewing, "viewing", "Event viewing", EXCL, SYNC, STOP);
	canspy_service_register(CANSPY_UART, canspy_uart_shell, "shell", "Interactive shell", EXCL, SYNC, START);
	
	/* Lowering debug levels after the boot */
	CANSPY_DEBUG_UART_Print = DBG_FATAL;
	CANSPY_DEBUG_ETH_Print = DBG_FATAL;

  /* Starting the scheduler (do not return) */
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_12, GPIO_PIN_SET);
	canspy_sched_exec();
}
