/**
  ******************************************************************************
  * @file    canspy_filter.h
  * @author  Arnaud Lebrun <a-lebrun@live.fr>
  * @author  Jonathan-Christofer Demay <jcdemay@gmail.com>
  * @version V1.0
  * @brief   This file contains all the functions prototypes for the
  *          Filter module core.
  *
  ******************************************************************************
  * COPYRIGHT(c) 2016 Arnaud Lebrun and Jonathan-Christofer Demay
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of the copyright holder nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  ******************************************************************************
  */ 

#ifndef __CANSPY_FILTER_H
#define __CANSPY_FILTER_H

/* Includes ------------------------------------------------------------------*/
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include "stm32f4xx_can.h"
#include "slre.h"

/* Exported typedefs ---------------------------------------------------------*/
typedef enum _CANSPY_FILTER_INT{
	
	INT_LOW,
	INT_HIG,
	INT_EQU,
	INT_DIF,
	INT_ANY
}
CANSPY_FILTER_INT;

typedef enum _CANSPY_FILTER_CAN{
	
	CAN_ANY,
	CAN_IF1 = CAN1_IF,
	CAN_IF2 = CAN2_IF,
	CAN_ALL = CANA_IF /* for replay purposes */
}
CANSPY_FILTER_CAN;

typedef enum _CANSPY_FILTER_RTR{
	
	RTR_DAT = CAN_RTR_DATA,
	RTR_ANY,
	RTR_REM = CAN_RTR_REMOTE,
}
CANSPY_FILTER_RTR;

typedef enum _CANSPY_FILTER_BUF{
	
	BUF_ANY,
	BUF_BEG,
	BUF_END,
	BUF_EQU,
	BUF_CON,
	BUF_REG
}
CANSPY_FILTER_BUF;

typedef enum _CANSPY_FILTER_ACT{
	
	ACT_DROP,
	ACT_ALTR,
	ACT_FWRD,
	ACT_ANY /* for capture purposes */
}
CANSPY_FILTER_ACT;

typedef struct _CANSPY_FILTER_RULE{
	
	uint8_t rul_id;
	CANSPY_FILTER_CAN can_if;
	uint32_t can_id;
	CANSPY_FILTER_INT cmp_id;
	CANSPY_FILTER_RTR can_rtr;
	uint32_t can_dlc;
	CANSPY_FILTER_INT cmp_dlc;
	char *can_data;
	uint16_t len_data;
	CANSPY_FILTER_BUF cmp_data;
	CANSPY_FILTER_ACT can_act;
	struct slre_cap *act_cap;
	struct slre_cap *can_cap;
	uint8_t cap_len;
}
CANSPY_FILTER_RULE;

typedef struct _CANSPY_FILTER_DESC{
	
	CANSPY_FILTER_RULE *rul_list;
	int rul_size;
}
CANSPY_FILTER_DESC;

/* Exported macros -----------------------------------------------------------*/
#define MIN(a, b) (((a) < (b)) ? (a) : (b))
#define MAX(a, b) (((a) > (b)) ? (a) : (b))

/* Exported globals ----------------------------------------------------------*/
extern CANSPY_FILTER_DESC fil_can;
extern char CANSPY_FILTER_INT_Char[];
extern char CANSPY_FILTER_ANY_String[];
extern char *CANSPY_FILTER_CAN_String[];
extern char *CANSPY_FILTER_RTR_String[];
extern char *CANSPY_FILTER_BUF_String[];
extern char *CANSPY_FILTER_ACT_String[];

/* Exported functions --------------------------------------------------------*/
int canspy_filter_register(CANSPY_FILTER_DESC *fil_ptr, CANSPY_FILTER_RULE **out_rul, int argc, char *argv[]);
int canspy_filter_print(void *handle, CANSPY_FILTER_RULE *rul_ptr);
int canspy_filter_free(CANSPY_FILTER_RULE *rul_ptr);
int canspy_filter_remove(CANSPY_FILTER_DESC *fil_ptr, uint8_t rul_id);
CANSPY_FILTER_RULE *canspy_filter_walk(CANSPY_FILTER_DESC *fil_ptr, CANSPY_FILTER_RULE *cur_rul);
int canspy_filter_match(int can_if, CANSPY_FILTER_RULE *fil_ptr, CanRxMsgTypeDef *pRxMess);
int canspy_filter_alter(CANSPY_FILTER_RULE *rul_ptr, CanTxMsgTypeDef *pCAN_Tx_Mess, CanRxMsgTypeDef *pCAN_Rx_Mess);

#endif /*_CANSPY_FILTER_H*/
