/**
  ******************************************************************************
  * @file    canspy_option.h
  * @author  Arnaud Lebrun <a-lebrun@live.fr>
  * @author  Jonathan-Christofer Demay <jcdemay@gmail.com>
  * @version V1.0
  * @brief   This file contains all the functions prototypes for the
  *          Option module core.
  *
  ******************************************************************************
  * COPYRIGHT(c) 2016 Arnaud Lebrun and Jonathan-Christofer Demay
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of the copyright holder nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  ******************************************************************************
  */ 

#ifndef __CANSPY_OPTION_H
#define __CANSPY_OPTION_H

/* Includes ------------------------------------------------------------------*/
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>

/* External typedefs ---------------------------------------------------------*/
typedef struct _CANSPY_DEVICE_DESC CANSPY_DEVICE_DESC;
typedef struct _CANSPY_SERVICE_DESC CANSPY_SERVICE_DESC;

/* Exported typedefs ---------------------------------------------------------*/
typedef enum _CANSPY_OPTION_TYPE{

	OPTION_BOOL,
	OPTION_STRING,
	OPTION_DEBUG,
	OPTION_MACADDR,
	OPTION_ETHTYPE,
	OPTION_PRESCALE,
	OPTION_CANIF,
	OPTION_ACTION,
	OPTION_ETHACK,
	OPTION_TYPES
}
CANSPY_OPTION_TYPE;

typedef struct _CANSPY_OPTION_CMP{
	
	int var_id;
	const char *var_name;
	const char *var_desc;
	const char *var_range;
	void *var_addr;
	char *var_string;
	int var_length;
	char *new_value;
	bool restart;
}
CANSPY_OPTION_CMP;

typedef struct _CANSPY_OPTION_DESC{
	
	CANSPY_OPTION_CMP *var_list;
	int var_size;
}
CANSPY_OPTION_DESC;

typedef union _CANSPY_OPTION_CALL{
	
	CANSPY_OPTION_CMP *var_ptr;
	CANSPY_DEVICE_DESC *dev_ptr;
	CANSPY_SERVICE_DESC *svc_ptr;
}
CANSPY_OPTION_CALL;

typedef enum _CANSPY_OPTION_REQ{
	
	OPT_SET = -6,
	OPT_GET = -5,
	OPT_STOP = -4,
	OPT_START = -3,
	OPT_POST = -2,
	OPT_INIT = -1
}
CANSPY_OPTION_REQ;

typedef int (*CANSPY_OPTION_FUNC)(int, CANSPY_OPTION_CALL *);

/* Exported macros -----------------------------------------------------------*/
#define CANSPY_OPTION_VAR (opt->var_ptr->var_addr)
#define CANSPY_OPTION_STR (opt->var_ptr->var_string)
#define CANSPY_OPTION_LEN (opt->var_ptr->var_length)
#define CANSPY_OPTION_NEW (opt->var_ptr->new_value)
#define CANSPY_OPTION_CMP(p) (CANSPY_OPTION_VAR == p)
#define CANSPY_OPTION_SET(f,v) canspy_option_##f(v, &CANSPY_OPTION_NEW, VAR_SET)
#define CANSPY_OPTION_GET(f,v) canspy_option_##f(v, &CANSPY_OPTION_STR, VAR_GET)
#define VAR_SET true
#define VAR_GET false

/* Exported globals ----------------------------------------------------------*/
extern char *CANSPY_OPTION_ERROR_Unknown;
extern char *CANSPY_OPTION_TYPES_Range[OPTION_TYPES];
extern int CANSPY_OPTION_TYPES_Length[OPTION_TYPES];
extern char *CANSPY_OPTION_BOOL_String[];

/* Exported functions --------------------------------------------------------*/
int canspy_option_register(CANSPY_OPTION_DESC *opt_ptr, const char *var_name, void *var_addr, const char* var_range, const char *var_desc, int var_length, bool restart);
CANSPY_OPTION_CMP *canspy_option_walk(CANSPY_OPTION_DESC *opt_ptr, CANSPY_OPTION_CMP *cur_var, void *opt_func);
CANSPY_OPTION_CMP *canspy_option_variable(bool write, char *var_name, char *val);
void canspy_option_bool(void *p1, void *p2, bool set);
void canspy_option_string(void *p1, void *p2, bool set);
void canspy_option_debug(void *in, void *out, bool set);
void canspy_option_macaddr(void *in, void *out, bool set);
void canspy_option_ethtype(void *in, void *out, bool set);
void canspy_option_ethack(void *p1, void *p2, bool set);
void canspy_option_prescale(void *in, void *out, bool set);
void canspy_option_canif(void *p1, void *p2, bool set);
void canspy_option_action(void *in, void *out, bool set);

#endif /*_CANSPY_OPTION_H*/
