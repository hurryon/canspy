/**
  ******************************************************************************
  * @file    canspy_print.h
  * @author  Arnaud Lebrun <a-lebrun@live.fr>
  * @author  Jonathan-Christofer Demay <jcdemay@gmail.com>
  * @version V1.0
  * @brief   This file contains all the functions prototypes for the
  *          Print module extension.
  *
  ******************************************************************************
  * COPYRIGHT(c) 2016 Arnaud Lebrun and Jonathan-Christofer Demay
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of the copyright holder nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  ******************************************************************************
  */ 
 
#ifndef __CANSPY_PRINT_H_
#define __CANSPY_PRINT_H_

/* Includes ------------------------------------------------------------------*/
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>

/* Exported constants --------------------------------------------------------*/
#define TAB_LEN 8
#define CANSPY_PRINT_BREAK "\r\n"
#define CANSPY_PRINT_PADCHAR "\t"
#define CANSPY_HANDLE_UART (void *)(1)
#define CANSPY_HANDLE_ETH (void *)(2)

/* Exported macros -----------------------------------------------------------*/
#define STRNILENCMP(s, v) strncasecmp((s), (v), strlen(s))
#define STRNICONSTCMP(s, v) strncasecmp((s), (v), sizeof(s))
#define STRINGIFY(x) #x
#define TOSTRING(x) STRINGIFY(x)

/* Exported globals ----------------------------------------------------------*/
extern uint8_t CANSPY_PRINT_PAD_Option;
extern uint8_t CANSPY_PRINT_PAD_Device;
extern uint8_t CANSPY_PRINT_PAD_Service;
extern uint8_t CANSPY_PRINT_PAD_Value;
extern uint8_t CANSPY_PRINT_PAD_Range;
extern int CANSPY_PRINT_MAX_Option;
extern int CANSPY_PRINT_MAX_Device;
extern int CANSPY_PRINT_MAX_Service;
extern int CANSPY_PRINT_MAX_Value;
extern int CANSPY_PRINT_MAX_Range;
extern int CANSPY_PRINT_MAX_Command;

/* Exported functions --------------------------------------------------------*/
int canspy_print_buffer(void *handle, void *buffer, int len);
int canspy_print_printable(void *handle, char single);
int canspy_print_string(void *handle, const char *string, int len);
int canspy_print_line(void *handle, const char *string, int len);
int canspy_print_format(void *handle, const char *format, ...);
int canspy_print_format_line(void *handle, const char *format, ...);
int canspy_print_format_buffer(char *buffer, int size, const char *format, ...);
char *canspy_print_format_alloc(const char *format, ...);
int canspy_print_padding(void *handle, const char *string, int len, int *p_max, int min);
int canspy_print_padding_string(void *handle, const char *string, int len, int *p_max, int min);

#endif /*_CANSPY_PRINT_H*/
