/**
  ******************************************************************************
  * @file    canspy_sdcard.h
  * @author  Arnaud Lebrun <a-lebrun@live.fr>
  * @author  Jonathan-Christofer Demay <jcdemay@gmail.com>
  * @version V1.0
  * @brief   This file contains all the functions prototypes for the
  *          SDCARD module core.
  *
  ******************************************************************************
  * COPYRIGHT(c) 2016 Arnaud Lebrun and Jonathan-Christofer Demay
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of the copyright holder nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  ******************************************************************************
  */ 
 
#ifndef __CANSPY_SDCARD_H_
#define __CANSPY_SDCARD_H_

/* Includes ------------------------------------------------------------------*/
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include "stm32f4xx_fatfs.h"
#include "canspy_sched.h"

/* External typedefs ---------------------------------------------------------*/
typedef struct _CANSPY_SDCARD_FIL{
	
	FIL fd;
	UINT buf_size;
	uint8_t *buf_ptr;	/* NULL when unbuffered */
	UINT buf_ind;			/* 0 for flush-only operations */
	UINT buf_len;			/* for read operation only */
}
CANSPY_SDCARD_FIL;

/* Exported constants --------------------------------------------------------*/
#define CANSPY_SDCARD_MAX_FILES 65460

/* Exported globals ----------------------------------------------------------*/
extern char CANSPY_SDCARD_Drive[4];
extern FATFS CANSPY_SDCARD_Filesystem;
extern char CANSPY_SDCARD_Path[8192];
extern CANSPY_SDCARD_FIL *CANSPY_SDCARD_Log;

/* Exported functions --------------------------------------------------------*/
int canspy_sdcard_device(CANSPY_DEVICE_CALL call, CANSPY_OPTION_CALL *opt);
void canspy_sdcard_init(CANSPY_SDCARD_FIL *bfp);
int canspy_sdcard_open(CANSPY_SDCARD_FIL *bfp, const char* path, BYTE mode);
int canspy_sdcard_read(CANSPY_SDCARD_FIL *bfp, void *buffer, int length, UINT *p_read);
int canspy_sdcard_write(CANSPY_SDCARD_FIL *bfp, void *buffer, int length, bool flush);
int canspy_sdcard_close(CANSPY_SDCARD_FIL *bfp);
int canspy_sdcard_capture(CANSPY_SERVICE_FLAG flag, CANSPY_OPTION_CALL *opt);
int canspy_sdcard_logging(CANSPY_SERVICE_FLAG flag, CANSPY_OPTION_CALL *opt);
int canspy_sdcard_replay(CANSPY_SERVICE_FLAG flag, CANSPY_OPTION_CALL *opt);

#endif /*_CANSPY_SDCARD_H*/
