/**
  ******************************************************************************
  * @file    canspy_can.h
  * @author  Arnaud Lebrun <a-lebrun@live.fr>
  * @author  Jonathan-Christofer Demay <jcdemay@gmail.com>
  * @version V1.0
  * @brief   This file contains all the functions prototypes for the
  *          CAN module core.
  *
  ******************************************************************************
  * COPYRIGHT(c) 2016 Arnaud Lebrun and Jonathan-Christofer Demay
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of the copyright holder nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  ******************************************************************************
  */ 
 
#ifndef __CANSPY_CAN_H_
#define __CANSPY_CAN_H_

/* Includes ------------------------------------------------------------------*/
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include "stm32f4xx_can.h"
#include "canspy_sched.h"
#include "canspy_filter.h"
#include "canspy_eth.h"

/* Exported typedefs ---------------------------------------------------------*/
typedef struct _SOCKETCAN_{
		
	uint32_t	id;  /* 32 bits ID + EFF/RTR/ERR flags  */
	uint8_t		dlc; /* payload length in byte (0 to 8) */
	uint8_t		__pad;   /* padding  */
	uint8_t		__res0;  /* reserved */
	uint8_t		__res1;  /* reserved */
	uint8_t		data[8] __attribute__((aligned(8))); /* payload */
}
SOCKETCAN;

/* Exported constants --------------------------------------------------------*/
#define SOCKETCAN_EFF 0x80000000
#define SOCKETCAN_RTR 0x40000000
#define SOCKETCAN_ERR 0x20000000
#define SOCKETCAN_FLAG (SOCKETCAN_EFF|SOCKETCAN_RTR|SOCKETCAN_ERR)
#define SOCKETCAN_MASK (~SOCKETCAN_FLAG)
#define canspy_can_handler_rx CAN_Rx_Handler
#define canspy_can_handler_tx CAN_Tx_Handler

/* Exported macros -----------------------------------------------------------*/
#define CANSPY_CANDEV_OPTION(IF, NAME, RANGE, SIZE, ADDR, DESC, RESTART) CANSPY_DEVICE_OPTION(canspy_print_format_alloc(NAME, IF), RANGE, SIZE, ADDR, canspy_print_format_alloc(DESC, IF), RESTART)
#define CANSPY_CANSVC_OPTION(IF, NAME, RANGE, SIZE, ADDR, DESC, RESTART) CANSPY_SERVICE_OPTION(canspy_print_format_alloc(NAME, IF), RANGE, SIZE, ADDR, canspy_print_format_alloc(DESC, IF), RESTART)

/* Exported globals ----------------------------------------------------------*/
extern CanRxMsgTypeDef *CANSPY_CAN_Rx_Mess[];
extern CanTxMsgTypeDef *CANSPY_CAN_Tx_Mess[];
extern CanRxMsgTypeDef *CANSPY_CAN_RxTx_Mess[];
extern CanTxMsgTypeDef *CANSPY_CAN_TxRx_Mess[];
extern CANSPY_FILTER_ACT CANSPY_CAN_Match[];
extern CANSPY_ETH_ACK CANSPY_INJ_ETH_Ack;
extern uint8_t CANSPY_INJ_ETH_If;
extern uint8_t CANSPY_INJ_SVC_If;
extern uint8_t CANSPY_INJ_UART_If;
extern SOCKETCAN CANSPY_INJ_ETH_Sock;
extern SOCKETCAN CANSPY_INJ_SVC_Sock;
extern SOCKETCAN CANSPY_INJ_UART_Sock;
extern bool CANSPY_INJ_ETH_Async;
extern bool CANSPY_INJ_SVC_Async;
extern bool CANSPY_INJ_UART_Async;
extern CANSPY_SERVICE_DESC *CANSPY_INJ_Services[];
#ifdef CANSPY_DEBUG_FILTER
	extern bool filtered_forwarding[];
	extern bool silent_fwrd[];
	extern bool dummy_drop[];
	extern uint16_t match_rule_s_id[];
	extern uint16_t match_rule_d_id[];
#endif

/* Exported functions --------------------------------------------------------*/
uint16_t canspy_can_prescaler(uint16_t val, bool reverse);
int canspy_can_device1(CANSPY_DEVICE_CALL call, CANSPY_OPTION_CALL *opt);
int canspy_can_device2(CANSPY_DEVICE_CALL call, CANSPY_OPTION_CALL *opt);
void canspy_can_silence(CAN_HandleTypeDef *pCAN_Hdl, bool silent);
int canspy_can_variables(uint8_t can, HAL_StatusTypeDef (**ppMX_CAN_Init)(void), CANSPY_SERVICE_FLAG *pCAN_Rx_Flag, CanRxMsgTypeDef **ppCAN_Rx_Mess, CanTxMsgTypeDef **ppCAN_Tx_Mess, CAN_HandleTypeDef **ppCAN_Hdl, uint16_t **ppCAN_Prescaler);
void canspy_can_wrap(SOCKETCAN *pcan_frame, CanRxMsgTypeDef *pRxMess, bool network);
void canspy_can_unwrap(CanRxMsgTypeDef *pRxMess, SOCKETCAN *pcan_frame, bool network);
SOCKETCAN *canspy_can_rewrap(SOCKETCAN *pcan_frame);
int canspy_can_filter1(CANSPY_SERVICE_FLAG flag, CANSPY_OPTION_CALL *opt);
int canspy_can_forward1(CANSPY_SERVICE_FLAG flag, CANSPY_OPTION_CALL *opt);
int canspy_can_inject1(CANSPY_SERVICE_FLAG flag, CANSPY_OPTION_CALL *opt);
int canspy_can_filter2(CANSPY_SERVICE_FLAG flag, CANSPY_OPTION_CALL *opt);
int canspy_can_forward2(CANSPY_SERVICE_FLAG flag, CANSPY_OPTION_CALL *opt);
int canspy_can_inject2(CANSPY_SERVICE_FLAG flag, CANSPY_OPTION_CALL *opt);

#endif /*_CANSPY_CAN_H*/
