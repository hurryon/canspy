/**
  ******************************************************************************
  * @file    canspy_cmd.h
  * @author  Arnaud Lebrun <a-lebrun@live.fr>
  * @author  Jonathan-Christofer Demay <jcdemay@gmail.com>
  * @version V1.0
  * @brief   This file contains all the functions prototypes for the
  *          CMD module extension.
  *
  ******************************************************************************
  * COPYRIGHT(c) 2016 Arnaud Lebrun and Jonathan-Christofer Demay
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of the copyright holder nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  ******************************************************************************
  */ 
 
#ifndef __CANSPY_CMD_H_
#define __CANSPY_CMD_H_

/* Includes ------------------------------------------------------------------*/
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <stdarg.h>
#include "canspy_sched.h"

/* Exported functions --------------------------------------------------------*/
void canspy_cmd_help(int argc, char **argv);
void canspy_cmd_stats(int argc, char **argv);
void canspy_cmd_device(int argc, char **argv);
void canspy_cmd_service(int argc, char **argv);
void canspy_cmd_option(int argc, char **argv);
void canspy_cmd_filter(int argc, char **argv);
void canspy_cmd_inject(int argc, char **argv);
void canspy_cmd_ls(int argc, char **argv);
void canspy_cmd_rm(int argc, char **argv);
void canspy_cmd_mv(int argc, char **argv);
void canspy_cmd_cd(int argc, char **argv);
void canspy_cmd_pwd(int argc, char **argv);
void canspy_cmd_cat(int argc, char **argv);
void canspy_cmd_xxd(int argc, char **argv);
void canspy_cmd_mkdir(int argc, char **argv);
void canspy_cmd_rmdir(int argc, char **argv);
void canspy_cmd_pcap(int argc, char **argv);

#endif /*_CANSPY_CMD_H*/
