/**
  ******************************************************************************
  * @file    canspy_pcap.h
  * @author  Arnaud Lebrun <a-lebrun@live.fr>
  * @author  Jonathan-Christofer Demay <jcdemay@gmail.com>
  * @version V1.0
  * @brief   This file contains all the functions prototypes for the
  *          PCAP module extension.
  *
  ******************************************************************************
  * COPYRIGHT(c) 2016 Arnaud Lebrun and Jonathan-Christofer Demay
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of the copyright holder nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  ******************************************************************************
  */ 
 
#ifndef __CANSPY_PCAP_H_
#define __CANSPY_PCAP_H_

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>
#include <stdio.h>
#include "canspy_sdcard.h"
#include "canspy_can.h"

/* Exported typedefs ---------------------------------------------------------*/
typedef struct _CANSPY_PCAP_HEADER_GLOBAL{
	
	uint32_t magic;
	uint16_t major;
	uint16_t minor;
	uint32_t timezone;
	uint32_t accuracy;
	uint32_t max_length;
	uint32_t link_layer;
}
CANSPY_PCAP_HEADER_GLOBAL;

typedef struct _CANSPY_PCAP_HEADER_RECORD{
	
	uint32_t ts_sec;
	uint32_t ts_usec;
	uint32_t incl_len;
	uint32_t orig_len;
}
CANSPY_PCAP_HEADER_RECORD;

/* Exported globals ----------------------------------------------------------*/
extern CANSPY_PCAP_HEADER_GLOBAL CANSPY_PCAP_FILE_Header;

/* Exported functions --------------------------------------------------------*/
int canspy_pcap_check(CANSPY_SDCARD_FIL *fp);
int canspy_pcap_walk(CANSPY_SDCARD_FIL *fp, SOCKETCAN *cap_sock, CANSPY_PCAP_HEADER_RECORD *out_header, uint8_t *can_if);
int canspy_pcap_write(CANSPY_SDCARD_FIL *fp, SOCKETCAN *pcan_frame, CANSPY_PCAP_HEADER_RECORD *rec_header, uint8_t can_if);

#endif /*_CANSPY_PCAP_H*/
