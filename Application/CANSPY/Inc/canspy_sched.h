/**
  ******************************************************************************
  * @file    canspy_sched.h
  * @author  Arnaud Lebrun <a-lebrun@live.fr>
  * @author  Jonathan-Christofer Demay <jcdemay@gmail.com>
  * @version V1.0
  * @brief   This file contains all the functions prototypes for the
  *          Sched module core.
  *
  ******************************************************************************
  * COPYRIGHT(c) 2016 Arnaud Lebrun and Jonathan-Christofer Demay
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of the copyright holder nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  ******************************************************************************
  */ 

#ifndef __CANSPY_SCHED_H
#define __CANSPY_SCHED_H

/* Includes ------------------------------------------------------------------*/
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <stdarg.h>
#include "stm32f4xx_gpio.h"
#include "canspy_option.h"
#include "canspy_device.h"
#include "canspy_service.h"

/* Exported constants---------------------------------------------------------*/
#define EXCL true
#define SHARE false
#define ASYNC true
#define SYNC false
#define START true
#define STOP false
#define RESTART true
#define INSTANT false

/* Exported macros -----------------------------------------------------------*/
#define CANSPY_SCHED_REGISTER_CAN(DEV, ID, PROC, NAME, DESC, EXCL, ASYNC, STATE) canspy_service_register(DEV ## ID, PROC ## ID, NAME #ID, DESC #ID, EXCL, ASYNC, STATE)
#define CANSPY_SCHED_REGISTER_CAN_ALL(DEV, PROC, NAME, DESC, EXCL, ASYNC, STATE) {CANSPY_SCHED_REGISTER_CAN(DEV, 1, PROC, NAME, DESC, EXCL, ASYNC, STATE); CANSPY_SCHED_REGISTER_CAN(DEV, 2, PROC, NAME, DESC, EXCL, ASYNC, STATE);}
#define CANSPY_SCHED_SIGNAL(FLAG) {CANSPY_SCHED_Flags[FLAG]++; CANSPY_SCHED_Interrupt = true;}
#define CANSPY_SCHED_SIGNAL2(FLAG, INC) {CANSPY_SCHED_Flags[FLAG] += INC; CANSPY_SCHED_Interrupt = true;}

/* Exported globals ----------------------------------------------------------*/
extern CANSPY_DEVICE_DESC CANSPY_SCHED_Devices[CANSPY_DEVICES];
extern uint16_t CANSPY_SCHED_Flags[CANSPY_FLAGS];
extern bool CANSPY_SCHED_Interrupt;
extern bool CANSPY_SCHED_Running;

/* Exported functions --------------------------------------------------------*/
void canspy_sched_init(void);
void canspy_sched_exec(void);
void canspy_sched_consume(CANSPY_SERVICE_FLAG flag);
void *canspy_sched_buffer(CANSPY_SERVICE_FLAG flag);

#endif /*_CANSPY_SCHED_H*/
