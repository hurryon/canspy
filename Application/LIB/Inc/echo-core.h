/* Derived from echo.c in Core Utilities, derived from echo.c in Bash.
   echo-core.h -- prototypes used by echo.
   Copyright (C) 2000-2016 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

#ifndef ECHO_CORE_H
# define ECHO_CORE_H

int
unescape (char *output, char *input);

int
escape (char *output, char *input, int length);

#endif /* ECHO_CORE_H */
