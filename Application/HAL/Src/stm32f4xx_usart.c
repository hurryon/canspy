/**
  ******************************************************************************
  * File Name          : stm32f4xx_usart.c
  * Description        : This file provides code for the configuration
  *                      of the USART instances.
  ******************************************************************************
  *
  * COPYRIGHT(c) 2015 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

#include "stm32f4xx_usart.h"

/** @brief  Handle for UART6.
  */
UART_HandleTypeDef huart6;

/** @brief  Buffer for UART RX.
  */
uint8_t UART_Rx_Buf[UART_RX_BUF_SIZE];

/** @brief  Iterator for UART RX.
  */
uint32_t UART_Rx_Iter = 0;

/**
  * @brief  This function initializes UART6.
  */
HAL_StatusTypeDef MX_USART6_UART_Init(void){

  huart6.Instance = USART6;
  huart6.Init.BaudRate = 115200;
  huart6.Init.WordLength = UART_WORDLENGTH_8B;
  huart6.Init.StopBits = UART_STOPBITS_1;
  huart6.Init.Parity = UART_PARITY_NONE;
  huart6.Init.Mode = UART_MODE_TX_RX;
  huart6.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart6.Init.OverSampling = UART_OVERSAMPLING_16;
	
  return HAL_UART_Init(&huart6);
}

/**
  * @brief  This function initializes UART MSP.
  */
void HAL_UART_MspInit(UART_HandleTypeDef* huart){
	
  GPIO_InitTypeDef GPIO_InitStruct;

  if(huart->Instance==USART6){

    /* Peripheral clock enable */
    __USART6_CLK_ENABLE();
  
    /**USART6 GPIO Configuration    
    PC6     ------> USART6_TX
    PC7     ------> USART6_RX 
    */
    GPIO_InitStruct.Pin = GPIO_PIN_6|GPIO_PIN_7;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    GPIO_InitStruct.Speed = GPIO_SPEED_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF8_USART6;
    HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

    /* Peripheral interrupt init */
    HAL_NVIC_SetPriority(USART6_IRQn, 5, 0);
    HAL_NVIC_EnableIRQ(USART6_IRQn);
  }
}

/**
  * @brief  This function deinitializes UART MSP.
  */
void HAL_UART_MspDeInit(UART_HandleTypeDef* huart){

  if(huart->Instance==USART6){

    /* Peripheral clock disable */
    __USART6_CLK_DISABLE();
  
    /**USART6 GPIO Configuration    
    PC6     ------> USART6_TX
    PC7     ------> USART6_RX 
    */
    HAL_GPIO_DeInit(GPIOC, GPIO_PIN_6|GPIO_PIN_7);

    /* Peripheral interrupt Deinit*/
    HAL_NVIC_DisableIRQ(USART6_IRQn);
  }
} 

/**
  * @brief  This function handles USART6 global interrupt.
  */
void USART6_IRQHandler(void){
	
	/* TX interrupts do not require any processing */
	if(!__HAL_UART_GET_IT_SOURCE(&huart6, UART_IT_TXE) && !__HAL_UART_GET_IT_SOURCE(&huart6, UART_IT_TC)){
		
		/* Signaling that the IRQ has been handled */
		HAL_UART_IRQHandler(&huart6);

		/* Handling the IRQ and echoing the received character if specified */
		if(UART_Rx_Handler(UART_Rx_Buf[UART_Rx_Iter]) == 0){
			
			while(HAL_UART_Transmit(&huart6, UART_Rx_Buf + UART_Rx_Iter, 1, UART_TX_TIMEOUT) != HAL_OK);
		}
		
		UART_Rx_Iter++;
		
		/* Resetting the interrupt when UART buffer is full */
		if(UART_Rx_Iter >= sizeof(UART_Rx_Buf)) {
			
			UART_Rx_Iter = 0;
			HAL_UART_Receive_IT(&huart6, UART_Rx_Buf, sizeof(UART_Rx_Buf));
		}
	}
	else{
		
		/* Signaling that the IRQ has been handled */
		HAL_UART_IRQHandler(&huart6);
	}
}

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
